#pragma once
#include <windows.h>
#include <string>
#include <stdio.h>
#include <conio.h>
#include <tchar.h>

class Pipe
{
public:

    //constructor
    Pipe();
    
    //methods
    
    bool connect();
    void close();

    std::string getMesege();
    bool sendMesege(char* msg);

private:
    HANDLE hPipe;
    LPTSTR  strPipeName;
};