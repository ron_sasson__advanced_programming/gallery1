#pragma once
#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <Windows.h>
#include<iostream>
#include <string.h>
#include <stdlib.h>

#define PAINT_CHOICE 1
#define IRFAN_VIEW_CHOICE 2


class OpenDraw
{
public:
	OpenDraw();
	~OpenDraw();

	void openDrowing(std::string PathPicture);
	void closeProcess();

private:
	void openApp(std::string PathPicture, std::string appName);

	void printManu();
	int getNumberFromUser();

	void checkChangeOnPicture(PROCESS_INFORMATION processInfo, HANDLE hNotify);

	LPPROCESS_INFORMATION _processInfo;
};

