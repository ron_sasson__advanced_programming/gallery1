#include "PipeClient.h"

Pipe::Pipe()
{
    strPipeName = TEXT("\\\\.\\pipe\\Gallery");
}

bool Pipe::connect()
{
	hPipe = CreateFile(
        strPipeName,			// Pipe name 
        GENERIC_READ |			// Read and write access 
        GENERIC_WRITE,
        0,						// No sharing 
        NULL,					// Default security attributes
        OPEN_EXISTING,			// Opens existing pipe 
        0,						// Default attributes 
        NULL);

	// Break if the pipe handle is valid. 
	if (hPipe != INVALID_HANDLE_VALUE)
		return true;

	if (// Exit if an error other than ERROR_PIPE_BUSY occurs
		GetLastError() != ERROR_PIPE_BUSY
		||
		// All pipe instances are busy, so wait for 5 seconds
		!WaitNamedPipe(strPipeName, 5000))
	{
		_tprintf(_T("Unable to open named pipe %s w/err 0x%08lx\n"),
			strPipeName, GetLastError());
		return false;
	}

	_tprintf(_T("The named pipe, %s, is connected.\n"), strPipeName);
	return true;
}

void Pipe::close()
{
	CloseHandle(hPipe);
}

std::string Pipe::getMesege()
{
	DWORD cbBytesRead;
	DWORD cbReplyBytes;
	TCHAR chReply[1024];		// Server -> Client

	cbReplyBytes = sizeof(TCHAR) * 1024;
	BOOL bResult = ReadFile(			// Read from the pipe.
		hPipe,					// Handle of the pipe
		chReply,				// Buffer to receive the reply
		cbReplyBytes,			// Size of buffer 
		&cbBytesRead,			// Number of bytes read 
		NULL);					// Not overlapped 

	if (!bResult && GetLastError() != ERROR_MORE_DATA)
	{
		_tprintf(_T("ReadFile failed w/err 0x%08lx\n"), GetLastError());
		return "";
	}

	_tprintf(_T("Receives %ld bytes; Message: \"%s\"\n"),
		cbBytesRead, chReply);

	return chReply;

}

bool Pipe::sendMesege(char* msg)
{
	//char ea[] = "SSS";
	char* chRequest = msg;	// Client -> Server
	DWORD cbBytesWritten, cbRequestBytes;

	// Send one message to the pipe.

	cbRequestBytes = sizeof(TCHAR) * (lstrlen(chRequest) + 1);

	BOOL bResult = WriteFile(			// Write to the pipe.
		hPipe,						// Handle of the pipe
		chRequest,					// Message to be written
		cbRequestBytes,				// Number of bytes to write
		&cbBytesWritten,			// Number of bytes written
		NULL);						// Not overlapped 

	if (!bResult/*Failed*/ || cbRequestBytes != cbBytesWritten/*Failed*/)
	{
		_tprintf(_T("WriteFile failed w/err 0x%08lx\n"), GetLastError());
		return false;
	}

	_tprintf(_T("Sends %ld bytes; Message: \"%s\"\n"),
		cbBytesWritten, chRequest);

	return true;
}
