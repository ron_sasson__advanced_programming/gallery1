#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include "AlbumManager.h"
#include "DatabaseAccess.h"
#include "DataAccessTest.h"
#include "PipeClient.h"

DatabaseAccess* closeApp; // this function close varubal in case of ctrl + c

int getCommandNumberFromUser()
{
	std::string message("\nPlease enter any command(use number): ");
	std::string numericStr("0123456789");
	
	std::cout << message << std::endl;
	std::string input;
	std::getline(std::cin, input);
	
	while (std::cin.fail() || std::cin.eof() || input.find_first_not_of(numericStr) != std::string::npos) {

		std::cout << "Please enter a number only!" << std::endl;

		if (input.find_first_not_of(numericStr) == std::string::npos) {
			std::cin.clear();
		}

		std::cout << std::endl << message << std::endl;
		std::getline(std::cin, input);
	}
	
	return std::atoi(input.c_str());
}

bool consoleHandler(int signal)
{
	if (signal == CTRL_C_EVENT)
	{
		closeApp->killApp();
	}

	return true;
}

void printSystemInfo()
{
	time_t now = time(nullptr);
	std::stringstream oss;
	oss << std::put_time(localtime(&now), "%d/%m/%Y %H:%M:%S");

	std::cout << "time is: " + oss.str() << std::endl;  // print time
	std::cout << "Made by: Ron Sasson" << std::endl;
	std::cout << "version 1.0.3" << std::endl;
}

int main(void) // version 1.0.3
{
	// initialization data access
	DatabaseAccess dataAccess;

	

	// Avoid ctrl + c in app
	closeApp = &dataAccess;
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)consoleHandler, TRUE);

	// initialize album manager4
	AlbumManager albumManager(dataAccess);

	

	std::string albumName;
	std::cout << "Welcome to Gallery!" << std::endl;
	std::cout << "===================" << std::endl;
	std::cout << "Type " << HELP << " to a list of all supported commands" << std::endl;
	
	printSystemInfo();


	do {
		//int commandNumber = getCommandNumberFromUser();
		
		try	{
			albumManager.executeCommand();  //in this function he wait for mesege
		} catch (std::exception& e) {	
			std::cout << e.what() << std::endl;
		}
	} 
	while (true);
}


