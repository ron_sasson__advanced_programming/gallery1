﻿#pragma once
#include <vector>
#include "Constants.h"
#include "Album.h"
#include "DatabaseAccess.h"
#include "OpenDraw.h"
#include "PipeClient.h"

class AlbumManager
{
public:
	AlbumManager(DatabaseAccess& dataAccess);

	void executeCommand();
	void printHelp() const;

	using handler_func_t = void (AlbumManager::*)(void);    

private:
	std::string _msg;
	Pipe* m_pipeControler;
    int m_nextPictureId{};
    int m_nextUserId{};
    std::string m_currentAlbumName{};
	DatabaseAccess& m_dataAccess;
	Album m_openAlbum;

	void help();
	// albums management
	void createAlbum();
	void openAlbum();
	void closeAlbum();
	void deleteAlbum();
	void listAlbums();
	void listAlbumsOfUser();

	// Picture management
	void addPictureToAlbum();
	void removePictureFromAlbum();
	void listPicturesInAlbum();
	void showPicture();
	void changePicture();

	// tags related
	void tagUserInPicture();
	void untagUserInPicture();
	void listUserTags();

	// users management
	void addUser();
	void removeUser();
	void listUsers();
	void userStatistics();

	// Queries
	void topTaggedUser();
	void topTaggedPicture();
	void picturesTaggedUser();

	// ******************* Help & exit ******************* 
	void exit();
	std::vector<std::string> getParameterFromMesege();
	std::string getInputFromConsole(const std::string& message);
	bool fileExistsOnDisk(const std::string& filename);
	void refreshOpenAlbum();
    bool isCurrentAlbumSet() const;

	static const std::vector<struct CommandGroup> m_prompts;
	static const std::map<CommandType, handler_func_t> m_commands;

};

