#include "DatabaseAccess.h"

// album related

/*
the function return all the album in the db
input: nane
.
output: list of album class
.
*/

const std::list<Album> DatabaseAccess::getAlbums()
{
	std::list<Album> allAlbums;
	for (auto user: m_users)
	{
		for (auto album : getAlbumsOfUser(user)) //get all album of user and input hem on 1 list 
		{
			allAlbums.push_back(album);
		}
	}

	return allAlbums;
}

/*
the function return all the album of specific user
input: the user we wont to get is albums (class user)
.
output: list of albums of the user
.
*/

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	return writeSQL::openAlbumsDB(user.getId(), _db);
}

/*
the function creat album in the db
input:the album to adding for db (class album)
.
output: nane
.
*/

void DatabaseAccess::createAlbum(const Album& album)
{
	std::string comment = "INSERT INTO ALBUMS VALUES (";
	comment += std::to_string(getLastIdOFAlbum() + 1) + ", '"; // add album id
	comment += album.getName() + "', ";               // add album name
	comment += std::to_string(album.getOwnerId()) + ", '";            // add user id
	comment += album.getCreationDate() + "'";        // add creation date
	comment += ");";

	writeSQL::writeCommantToDB(comment, _db);
}

/*
the function delet album form the db
input: name of album (string), id of the user his album (int)
.
output: nane
.
*/

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	std::string comment = "DELETE FROM ALBUMS WHERE USER_ID = " + std::to_string(userId) + " AND NAME = '" + albumName + "';";

	writeSQL::writeCommantToDB(comment, _db);
}

/*
the function check if the album is exist
input: name of album (string), id of the user his album (int)
.
output: bool if he exsist
.
*/

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	bool checkExsist = false;

	for (auto album : getAlbums())  // serche in all albums
	{
		if (album.getName() == albumName && album.getOwnerId() == userId) // check if the album is eqal to the album input
		{
			checkExsist = true;
		}
	}

	return checkExsist;
}

/*
the function create album in the memory and return is refrence
input:the name of albums (string)
.
output: refrense of the album
.
*/

Album DatabaseAccess::openAlbum(const std::string& albumName, int userId)
{
	for (auto album : getAlbums())
	{
		if (album.getName() == albumName && album.getOwnerId() == userId) // check if the album is eqal to the album input
		{
			return album;
		}
	}

}

/*
the function relse the memory of the albums
input: the albums the relese is memory (class album)
.
output:
.
*/

void DatabaseAccess::closeAlbum(Album& pAlbum)
{
	pAlbum = Album();
}

/*
the function print all the albums in the db
input: nane
.
output: print to screen all the albums
.
*/

void DatabaseAccess::printAlbums()
{
	std::list<Album> albums;

	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (auto album : getAlbums())
		std::cout << std::setw(5) << "* " << album;
			
	
}

// picture related

/*
the function input new picture to album 
input: the name of album (string), picture(refrence to class Picture)
.
output: nane
.
*/

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture, int albumId)
{
	std::string comment = "INSERT INTO PICTURES VALUES (";
	comment += std::to_string(picture.getId()) + ", '"; // add picture id
	comment += picture.getName() + "', '";              // add picture name
	comment += picture.getPath() + "', '";              // add path for the picture
	comment += picture.getCreationDate() + "', ";       // add creation date
	comment += std::to_string(albumId);
	comment += ");";

	writeSQL::writeCommantToDB(comment, _db);
}

/*
the function remove form album a picture
input: the name of album (string), picture(refrence to class Picture)
.
output: nane
.
*/

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName, int albumId)
{
	std::string comment = "DELETE FROM PICTURES WHERE ALBUM_ID = " + std::to_string(albumId) + " AND NAME = '" + pictureName + "';";

	writeSQL::writeCommantToDB(comment, _db);
}

/*
the function tag user in pucture
input: name of albums where the picture are been(string), name of picture (string), user id who wont to tag
.
output: nane
.
*/

void DatabaseAccess::tagUserInPicture(const int albumId, const std::string& pictureName, int userId)
{
	int pictureId = -1;
	for (auto album : getAlbums())
	{
		if (album.getId() == albumId) // check if this album
		{
			for (auto picture : album.getPictures()) // serche the picture
			{
				if (picture.getName() == pictureName)
				{
					pictureId = picture.getId();
				}
			}
		}
	}

	if (pictureId != -1)
	{
		std::string comment = "INSERT INTO TAGS VALUES (";
		comment += std::to_string(pictureId) + ", ";
		comment += std::to_string(userId);
		comment += ");";

		writeSQL::writeCommantToDB(comment, _db);
	}
}

/*
the function remove tag from picture
input: name of albums where the picture are been(string), name of picture (string), user id who wont to tag
.
output: nane
.
*/

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	int idOfPicture = 0;
	for (auto album : getAlbums())
	{
		if (album.getName() == albumName)
		{
			idOfPicture = album.getPicture(pictureName).getId();
			break;
		}
	}
	std::string comment = "DELETE FROM TAGS WHERE USER_ID = " + std::to_string(userId) + " AND PICTURE_ID = " + std::to_string(idOfPicture) + ";";

	writeSQL::writeCommantToDB(comment, _db);
}

int DatabaseAccess::getLastPictureId()
{
	return writeSQL::getLastPictureId(_db);
}

/*
the function change the name of picture
input:album id and picture name in string
.
output: nane
.
*/
void DatabaseAccess::changeNameOfPicture(const int albumId, const std::string& pictureName, const std::string nameToChange)
{
	std::string comment = "update pictures set NAME = '" + nameToChange;
	comment += "' WHERE ALBUM_ID = " + std::to_string(albumId) + " AND";
	comment += " NAME = '" + pictureName + "';";

	writeSQL::writeCommantToDB(comment, _db);
}


// user related


/*
the function print all the user are axist
input: nane
.
output: nane (print for the screen all the user)
.
*/

void DatabaseAccess::printUsers()
{
	for (int i = 0; i < m_users.size(); i++)
		std::cout << std::to_string(i + 1) << ". " << m_users[i].getName() << std::endl;
	
}

/*
the function input all user in vector
input:vector of users
.
output: nane
.
*/

std::vector<User> DatabaseAccess::getUsers()
{
	return m_users;
}

/*
the function get the user by id
input: id of the user (int)
.
output: refrense of user class
.
*/

User DatabaseAccess::getUser(int userId)  /// not work!!
{
	for (auto user : m_users)
	{
		if (user.getId() == userId)  // check if the user id eqal to user id input
			return User(userId, user.getName());
	}

	// if he pass the loop it mean the user not found
	throw "function getUser";
}

/*
the function input the user into the db sql
input: user (refrense class User)
.
output: nane
.
*/

void DatabaseAccess::createUser(User& user)
{
	std::string comment = "INSERT INTO USERS "
						  "VALUES(";
	comment += std::to_string(user.getId());   // input id
	comment += ", '" + user.getName() + "');"; // input name

	writeSQL::writeCommantToDB(comment, _db);

	// if this comment dont throw error we add to user vector
	m_users.push_back(user);
}

/*
the function remove the user form db sql
input: user (refrense class user)
.
output: nane
.
*/

void DatabaseAccess::deleteUser(const User& user)
{
	bool checkRemove = false;
	int count = 0;

	for (auto userFromDb : m_users)
	{
		if (userFromDb == user)  // check if the user are exsist and then delete hem
		{
			std::string comment = "DELETE FROM USERS "
								  "WHERE ID = " + std::to_string(user.getId()) + ";";
			writeSQL::writeCommantToDB(comment, _db);

			//if not thorow exaption it mean the user remove
			m_users.erase(m_users.begin() + count);

			checkRemove = true;
		}
		count++;
	}

	if(!checkRemove) // if he pass the loop it mean the user not found 
		throw "function deleteUser";
	
	
}

/*
the function check if the user exist
input: user if for check
.
output: bool if the user exist
.
*/

bool DatabaseAccess::doesUserExists(int userId)
{
	for (auto user : m_users)
	{
		if (user.getId() == userId)  // check if the user id eqal to user id input
			return true;
	}

	// if he pass the loop it mean the user not found
	return false;
}

/*
the function return the last id to keep adding from this id
input: nane
.
output: last id (int)
.
*/

int DatabaseAccess::getLastUserId()
{
	return m_users[m_users.size() - 1].getId();
}


// user statistics

/*
the function count who much albums have to user
input: user (refrence to user class)
.
output: the number of the album (int)
.
*/

int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	return getAlbumsOfUser(user).size();
}

/*
the function count who much albums tag hem
input: user (refrence to user class)
.
output: number of tag (int)
.
*/

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	std::list<Picture> listOfPicture = writeSQL::getPictureBayId(_db, writeSQL::getPictureIdByUserId(_db, user.getId()));
	int count = 0;

	for (auto album : getAlbums())
	{
		if (isUserTagInThisAlbum(album, listOfPicture)) // check if 1 of this song in the album
			count++;
	}

	return count;
}

/*
the function count who much time tags the user
input: the user (refrence to class users)
.
output: number of tags (int)
.
*/

int DatabaseAccess::countTagsOfUser(const User& user)
{
	return writeSQL::getPictureIdByUserId(_db, user.getId()).size();
}

/*
the function avrge tag of user in album
input: user (refrence to user class)
.
output: the avrge of user in album (float)
.
*/

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	float avrage = 0;
	int albumTags = countAlbumsTaggedOfUser(user);

	if (albumTags > 0)
	{
		avrage = countTagsOfUser(user) / albumTags;
	}

	return avrage;
}


// queries

/*
the function return the user white the most tag
input: nane
.
output: the user white most of tag
.
*/

User DatabaseAccess::getTopTaggedUser()  // not work!!
{
	int userId = writeSQL::getTopUserTagged(_db);

	return getUser(userId);
}

/*
the function get the picture white most of tag
input: nane
.
output: picture white most of tag (refrnec to class Picture)
.
*/

Picture DatabaseAccess::getTopTaggedPicture()  // not work!!
{
	int pictureId = writeSQL::getTopAlbumTagged(_db);

	for (auto album : getAlbums()) // serche for the picture
	{
		for (auto picture : album.getPictures())
		{
			if (picture.getId() == pictureId)
			{
				return picture;
			}
		}
	}
}

/*
the function get all the picture where the user wast tag
input:the user
.
output:list of picture
.
*/

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	return writeSQL::getPictureBayId(_db, writeSQL::getPictureIdByUserId(_db, user.getId()));
}


// basic

/*
the function open the sql db
input: nane
.
output: if the sql can to open
.
*/

bool DatabaseAccess::open()
{
	std::string dbFileName = "galleryDB.sqlite";

	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &_db);

	if (res != SQLITE_OK)  // if can not open the sql file
	{
		_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		system("pause");
		throw "Failed to open DB";
	}
	
	// get users information

	writeSQL::openUsers(_db, this);

	draw = new OpenDraw();
}

/*
the function close the sql db
input:nane
.
output:nane
.
*/

void DatabaseAccess::close()
{
	sqlite3_close(_db);
	m_users.clear();
	draw->~OpenDraw();
	delete draw;
}

/*
the function clear all the memory we had for this class
input: nane
.
output: nane(relse the memory)
.
*/

void DatabaseAccess::clear()
{
	m_users.clear();
}


// app paint

// this function open the pricess of paint
void DatabaseAccess::openApp(std::string pathPucture)
{
	draw->openDrowing(pathPucture);
}

//this functin close the processs of paint
void DatabaseAccess::killApp()
{
	draw->closeProcess();
}


/*
the function this function check if the user ok and input hem
input: the user
.
output: nane
.
*/

void DatabaseAccess::addUser(User user)
{
	if (user.getName() != "") // check not empty name
	{
		for (int i = 0; i < m_users.size(); i++)
		{
			if (m_users[i].getName() == user.getName())  // if the users have the same name or the same id
				throw ("this name alredy used");  // if input worng

			else if (m_users[i].getId() == user.getId())
				throw("this id alredy used");
		}
		m_users.push_back(user);
	}
	
}


/*
the function get the last id from all album
input:nane
.
output: int of last id
.
*/

int DatabaseAccess::getLastIdOFAlbum()
{
	int higeId = 0;
	
	for (auto album : getAlbums())
	{
		if (album.getId() > higeId)
		{
			higeId = album.getId();
		}
	}

	return higeId;
}

/*
the function check if the user tag in 1 of picture of album
input:album, list of picture where the user tag
.
output:if is there
.
*/

bool DatabaseAccess::isUserTagInThisAlbum(Album album, std::list<Picture> pictureUserTag)
{
	for (auto picture : pictureUserTag)
	{
		if (album.doesPictureExists(picture.getName()))
			return true;
	}

	return false;
}


