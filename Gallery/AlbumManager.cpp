﻿#include "AlbumManager.h"
#include <iostream>
#include "Constants.h"
#include "MyException.h"
#include "AlbumNotOpenException.h"


AlbumManager::AlbumManager(DatabaseAccess& dataAccess) :
    m_dataAccess(dataAccess)
{
	// Left empty
	m_dataAccess.open();

	// get last id after open
	m_nextUserId = m_dataAccess.getLastUserId();

	m_nextPictureId = m_dataAccess.getLastPictureId();

	m_pipeControler = new Pipe();
	m_pipeControler->connect();
}

void AlbumManager::executeCommand() {
	/*
	mesege to server:
	4 first char are the number of mesege
	100# information why it dident success(work mesege)
	200# data (secced mesege)

	if return list********
	btween hvery elemnt have $ char 


	data:

	user:
	user_id|user_name
	200#10|ron|

	album:
	album_id|user_id|name|creationDate| 
	200#10|3|test|20/2/2002|


	picture:
	picture_id|name|path|creationDate| 
	200#3|bla|c:\blabla|28/10/2002|

	tags:  // ask for picture tags
	user_id1|user_id2|...|


	mesege from server:
	number of task(first 2 digit) and then parameter, end of every parameter have |

	xx parameter1|parameter2|parameter3|....

	*///Protocol of get mesege


	_msg = m_pipeControler->getMesege();
	int numOfMsege = (_msg[0] - 48) * 10; // first number of code
	numOfMsege += _msg[1] - 48;	  // second number of code
	//48 it mean the ascii number of zero
	
	try {
		AlbumManager::handler_func_t handler = m_commands.at(static_cast<CommandType>(numOfMsege));
		(this->*handler)();
	} catch (const std::out_of_range&) {
			throw MyException("Error: Invalid command[" + std::to_string(static_cast<CommandType>(numOfMsege)) + "]\n");
	}
}

void AlbumManager::printHelp() const
{
	std::cout << "Supported Album commands:" << std::endl;
	std::cout << "*************************" << std::endl;
	
	for (const struct CommandGroup& group : m_prompts) {
		std::cout << group.title << std::endl;
		std::string space(".  ");
		for (const struct CommandPrompt& command : group.commands) {
			space = command.type < 10 ? ".   " : ".  ";

			std::cout << command.type << space << command.prompt << std::endl;
		}
		std::cout << std::endl;
	}
}


// ******************* Album ******************* 
void AlbumManager::createAlbum()
{
	std::vector<std::string> parameters = getParameterFromMesege();
	std::string data = "200#";
	char dataToSend[1024] = { 0 };

	std::string userIdStr = parameters[0];
	int userId = std::stoi(userIdStr);
	if ( !m_dataAccess.doesUserExists(userId) ) {
		m_pipeControler->sendMesege("100#Error: Can't create album since there is no user with this id");
		throw MyException("Error: Can't create album since there is no user with id [" + userIdStr+"]\n");
	}

	std::string name = parameters[1];
	if ( m_dataAccess.doesAlbumExists(name,userId) ) {
		m_pipeControler->sendMesege("100#Error: Failed to create album, album with the same name already exists");
		throw MyException("Error: Failed to create album, album with the same name already exists\n");
	}

	Album newAlbum(userId,name);
	m_dataAccess.createAlbum(newAlbum);

	data += std::to_string(newAlbum.getId()) + "|";
	data += std::to_string(newAlbum.getOwnerId()) + "|";
	data += newAlbum.getName() + "|";
	data += newAlbum.getCreationDate() + "|";

	strcpy(dataToSend, data.c_str());
	m_pipeControler->sendMesege(dataToSend);
}

void AlbumManager::openAlbum()
{
	if (isCurrentAlbumSet()) {
		closeAlbum();
	}

	std::vector<std::string> parameters = getParameterFromMesege();

	std::string userIdStr = parameters[0];
	int userId = std::stoi(userIdStr);
	if ( !m_dataAccess.doesUserExists(userId) ) {
		m_pipeControler->sendMesege("100#Error: Can't open album since there is no user with this id");
		throw MyException("Error: Can't open album since there is no user with id @" + userIdStr + ".\n");
	}

	std::string name = parameters[1];
	if ( !m_dataAccess.doesAlbumExists(name, userId) ) {
		m_pipeControler->sendMesege("100#Error: Failed to open album, album with the same name already exists");
		throw MyException("Error: Failed to open album, since there is no album with name:"+name +".\n");
	}

	m_openAlbum = m_dataAccess.openAlbum(name, userId);
    m_currentAlbumName = name;
	// success
	std::cout << "Album [" << name << "] opened successfully." << std::endl;
	m_pipeControler->sendMesege("200#album open");
}

void AlbumManager::closeAlbum()
{
	refreshOpenAlbum();

	std::cout << "Album [" << m_openAlbum.getName() << "] closed successfully." << std::endl;
	m_dataAccess.closeAlbum(m_openAlbum);
	m_currentAlbumName = "";
	m_pipeControler->sendMesege("200#album close");
}

void AlbumManager::deleteAlbum()
{
	std::vector<std::string> parameters = getParameterFromMesege();

	std::string userIdStr = parameters[0];
	int userId = std::stoi(userIdStr);
	if (!m_dataAccess.doesUserExists(userId)) {
		m_pipeControler->sendMesege("100#Error: There is no user with this id ");
		throw MyException("Error: There is no user with id @" + userIdStr +"\n");
	}

	std::string albumName = parameters[1];
	if ( !m_dataAccess.doesAlbumExists(albumName, userId) ) {
		m_pipeControler->sendMesege("100#Error: Failed to delete album, since there is no album with this name:");
		throw MyException("Error: Failed to delete album, since there is no album with name:" + albumName + ".\n");
	}

	// album exist, close album if it is opened
	if ( (isCurrentAlbumSet() ) &&
		 (m_openAlbum.getOwnerId() == userId && m_openAlbum.getName() == albumName) ) {

		closeAlbum();
	}

	m_dataAccess.deleteAlbum(albumName, userId);
	std::cout << "Album [" << albumName << "] @"<< userId <<" deleted successfully." << std::endl;
	m_pipeControler->sendMesege("200#Album deleted successfully.");
}

void AlbumManager::listAlbums()
{
	std::list<Album> albums = m_dataAccess.getAlbums();
	std::string data;

	char dataToSend[1024] = { 0 };

	data = "200#";
	for (auto album : albums)
	{
		data += std::to_string(album.getId()) + "|";
		data += std::to_string(album.getOwnerId()) + "|";
		data += album.getName() + "|";
		data += album.getCreationDate() + "|";
		data += "$";
	}

	strcpy(dataToSend, data.c_str());
	m_pipeControler->sendMesege(dataToSend);
}

void AlbumManager::listAlbumsOfUser()
{
	std::vector<std::string> parameters = getParameterFromMesege();
	std::string data;
	char dataToSend[1024] = { 0 };

	std::string userIdStr = parameters[0];
	int userId = std::stoi(userIdStr);
	if (!m_dataAccess.doesUserExists(userId)) {
		m_pipeControler->sendMesege("100#Error: There is no user with this id ");
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	}

	const User& user = m_dataAccess.getUser(userId);
	const std::list<Album>& albums = m_dataAccess.getAlbumsOfUser(user);

	std::cout << "Albums list of user@" << user.getId() << ":" << std::endl;
	std::cout << "-----------------------" << std::endl;

	for (const auto& album : albums) {
		std::cout <<"   + [" << album.getName() <<"] - created on "<< album.getCreationDate() << std::endl;
	}

	data = "200#";
	for (auto album : albums)
	{
		data += std::to_string(album.getId()) + "|";
		data += std::to_string(album.getOwnerId()) + "|";
		data += album.getName() + "|";
		data += album.getCreationDate() + "|";
		data += "$";
	}

	strcpy(dataToSend, data.c_str());
	m_pipeControler->sendMesege(dataToSend);
}


// ******************* Picture ******************* 
void AlbumManager::addPictureToAlbum()
{
	refreshOpenAlbum();
	std::vector<std::string> parameters = getParameterFromMesege();
	std::string data = "200#";
	char dataToSend[1024] = { 0 };

	std::string picName = parameters[0];
	if (m_openAlbum.doesPictureExists(picName) ) {
		m_pipeControler->sendMesege("100#Error: Failed to add picture, picture with the same name already exists.");
		throw MyException("Error: Failed to add picture, picture with the same name already exists.\n");
	}
	
	Picture picture(++m_nextPictureId, picName);
	std::string picPath = parameters[1];
	picture.setPath(picPath);

	m_dataAccess.addPictureToAlbumByName(m_openAlbum.getName(), picture, m_openAlbum.getId());

	data += std::to_string(picture.getId()) + "|";
	data += picture.getName() + "|";
	data += picture.getPath() + "|";
	data += picture.getCreationDate() + "|";

	std::cout << "Picture [" << picture.getId() << "] successfully added to Album [" << m_openAlbum.getName() << "]." << std::endl;

	strcpy(dataToSend, data.c_str());
	m_pipeControler->sendMesege(dataToSend);
}

void AlbumManager::removePictureFromAlbum()
{
	refreshOpenAlbum();
	std::vector<std::string> parameters = getParameterFromMesege();

	std::string picName = parameters[0];
	if ( !m_openAlbum.doesPictureExists(picName) ) {
		m_pipeControler->sendMesege("100#Error: There is no picture withthis name");
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}
	
	auto picture = m_openAlbum.getPicture(picName);
	m_dataAccess.removePictureFromAlbumByName(m_openAlbum.getName(), picture.getName(), m_openAlbum.getId());

	std::cout << "Picture <" << picName << "> successfully removed from Album [" << m_openAlbum.getName() << "]." << std::endl;
	m_pipeControler->sendMesege("200#Picture removed added");
}

void AlbumManager::listPicturesInAlbum()
{
	refreshOpenAlbum();
	std::string data = "200#";
	char dataToSend[1024] = { 0 };

	std::cout << "List of pictures in Album [" << m_openAlbum.getName() 
			  << "] of user@" << m_openAlbum.getOwnerId() <<":" << std::endl;
	
	const std::list<Picture>& albumPictures = m_openAlbum.getPictures();
	for (auto iter = albumPictures.begin(); iter != albumPictures.end(); ++iter) {
		std::cout << "   + Picture [" << iter->getId() << "] - " << iter->getName() << 
			"\tLocation: [" << iter->getPath() << "]\tCreation Date: [" <<
				iter->getCreationDate() << "]\tTags: [" << iter->getTagsCount() << "]" << std::endl;

		///////////////////////add for app
		data += std::to_string(iter->getId()) + "|";
		data += iter->getName() + "|";
		data += iter->getPath() + "|";
		data += iter->getCreationDate() + "|";
		data += "$";
	}

	strcpy(dataToSend, data.c_str());
	m_pipeControler->sendMesege(dataToSend);

	std::cout << std::endl;
}

void AlbumManager::showPicture()
{
	refreshOpenAlbum();
	std::vector<std::string> parameters = getParameterFromMesege();

	std::string picName = parameters[0];
	if ( !m_openAlbum.doesPictureExists(picName) ) {
		m_pipeControler->sendMesege("100#Error: There is no picture withthis name");
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}
	
	auto pic = m_openAlbum.getPicture(picName);
	if ( !fileExistsOnDisk(pic.getPath()) ) {
		m_pipeControler->sendMesege("100#Error: Can't open");
		throw MyException("Error: Can't open <" + picName+ "> since it doesnt exist on disk.\n");
	}

	// Bad practice!!!
	// Can lead to privileges escalation
	// You will replace it on WinApi Lab(bonus)
	m_dataAccess.openApp(pic.getPath()); 
	m_pipeControler->sendMesege("200#picture showd");
}

void AlbumManager::changePicture()
{
	refreshOpenAlbum();
	std::vector<std::string> parameters = getParameterFromMesege();

	std::string picName = parameters[0];
	if (!m_openAlbum.doesPictureExists(picName)) {
		m_pipeControler->sendMesege("100#Error: There is no picture withthis name");
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}

	std::string changePicName = parameters[1];

	m_dataAccess.changeNameOfPicture(m_openAlbum.getId(), picName, changePicName);

	std::cout << "picture @" << picName << " successfully change to <" << changePicName << "> in album [" << m_openAlbum.getName() << "]" << std::endl;
	m_pipeControler->sendMesege("200#picture successfully change");
}

//tags

void AlbumManager::tagUserInPicture()
{
	refreshOpenAlbum();
	std::vector<std::string> parameters = getParameterFromMesege();

	std::string picName = parameters[0];
	if ( !m_openAlbum.doesPictureExists(picName) ) {
		m_pipeControler->sendMesege("100#Error: There is no picture withthis name");
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}
	
	Picture pic = m_openAlbum.getPicture(picName);
	
	std::string userIdStr = parameters[1];
	int userId = std::stoi(userIdStr);
	if ( !m_dataAccess.doesUserExists(userId) ) {
		m_pipeControler->sendMesege("100#Error: There is no user with this id");
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	}
	User user = m_dataAccess.getUser(userId);

	m_dataAccess.tagUserInPicture(m_openAlbum.getId(), pic.getName(), user.getId());

	std::cout << "User @" << userIdStr << " successfully tagged in picture <" << pic.getName() << "> in album [" << m_openAlbum.getName() << "]" << std::endl;
	m_pipeControler->sendMesege("200#User successfully tagged");

}

void AlbumManager::untagUserInPicture()
{
	refreshOpenAlbum();
	std::vector<std::string> parameters = getParameterFromMesege();

	std::string picName = parameters[0];
	if (!m_openAlbum.doesPictureExists(picName)) {
		m_pipeControler->sendMesege("100#Error: There is no picture withthis name");
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}

	Picture pic = m_openAlbum.getPicture(picName);

	std::string userIdStr = parameters[1];
	int userId = stoi(userIdStr);
	if (!m_dataAccess.doesUserExists(userId)) {
		m_pipeControler->sendMesege("100#Error: There is no user with this id");
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	}
	User user = m_dataAccess.getUser(userId);

	if (! pic.isUserTagged(user)) {
		m_pipeControler->sendMesege("100#Error: The user was not tagged");
		throw MyException("Error: The user was not tagged! \n");
	}

	m_dataAccess.untagUserInPicture(m_openAlbum.getName(), pic.getName(), user.getId());

	std::cout << "User @" << userIdStr << " successfully untagged in picture <" << pic.getName() << "> in album [" << m_openAlbum.getName() << "]" << std::endl;
	m_pipeControler->sendMesege("200#User successfully untagged");
}

void AlbumManager::listUserTags()
{
	std::string data = "200#";
	char dataToSend[1024] = { 0 };
	refreshOpenAlbum();
	std::vector<std::string> parameters = getParameterFromMesege();

	std::string picName = parameters[0];
	if ( !m_openAlbum.doesPictureExists(picName) ) {
		m_pipeControler->sendMesege("100#Error: There is no picture withthis name");
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}
	auto pic = m_openAlbum.getPicture(picName); 

	const std::set<int> users = pic.getUserTags();

	if ( 0 == users.size() )  {
		m_pipeControler->sendMesege("100#Error: There is no user tegged in this piccture");
		throw MyException("Error: There is no user tegged in <" + picName + ">.\n");
	}

	std::cout << "Tagged users in picture <" << picName << ">:" << std::endl;
	
	for (const int user_id: users) {
		const User user = m_dataAccess.getUser(user_id);
		std::cout << user << std::endl;

		data += std::to_string(user.getId()) + "|";
		data += user.getName() + "|$";
	}

	strcpy(dataToSend, data.c_str());
	m_pipeControler->sendMesege(dataToSend);
	std::cout << std::endl;
}


// ******************* User ******************* 
void AlbumManager::addUser()
{
	std::vector<std::string> parameters = getParameterFromMesege();
	std::string data = "200#";
	char dataToSend[1024] = { 0 };

	std::string name = parameters[0];

	for (auto user : m_dataAccess.getUsers())
	{
		if (user.getName() == name)
		{

			m_pipeControler->sendMesege("100#Error: There is no user user white this name");
			throw MyException("Error: There is user name alredy asxsist");
		}

	}

	User user(++m_nextUserId, name);

	m_dataAccess.createUser(user);
	data += std::to_string(user.getId());

	strcpy(dataToSend, data.c_str());
	m_pipeControler->sendMesege(dataToSend);

	std::cout << "User @" << m_nextUserId << " adding successfully." << std::endl;
}


void AlbumManager::removeUser()
{
	std::vector<std::string> parameters = getParameterFromMesege();

	// get user name
	std::string userIdStr = parameters[0];
	int userId = std::stoi(userIdStr);
	if (!m_dataAccess.doesUserExists(userId))
	{
		m_pipeControler->sendMesege("100#Error: There is no user with this id");
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	}
	
	const User& user = m_dataAccess.getUser(userId);
	const std::list<Album>& albums = m_dataAccess.getAlbumsOfUser(user);

	if (albums.size() > 0)  // check if have album
		for (auto album : albums) // pass in all album and delete hem
			m_dataAccess.deleteAlbum(album.getName(), userId);

	m_dataAccess.deleteUser(user);
	m_pipeControler->sendMesege("200#deleted successfully");

	std::cout << "User @" << userId << " deleted successfully." << std::endl;
}

void AlbumManager::listUsers()
{
	std::vector<User> users = m_dataAccess.getUsers();
	std::string data = "200#";
	char dataToSend[1024] = { 0 };

	for (auto user : users)
	{
		data += std::to_string(user.getId()) + "|";
		data += user.getName() + "|$";
	}

	strcpy(dataToSend, data.c_str());
	m_pipeControler->sendMesege(dataToSend);

}

void AlbumManager::userStatistics()
{
	std::vector<std::string> parameters = getParameterFromMesege();

	std::string userIdStr = parameters[0];
	int userId = std::stoi(userIdStr);
	if ( !m_dataAccess.doesUserExists(userId) ) {
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	}

	const User& user = m_dataAccess.getUser(userId);
	const std::list<Album>& albums = m_dataAccess.getAlbumsOfUser(user);

	std::cout << "user @" << userId << " Statistics:" << std::endl << "--------------------" << std::endl <<
		"  + Count of Albums Tagged: " << m_dataAccess.countAlbumsTaggedOfUser(user) << std::endl <<
		"  + Count of Tags: " << m_dataAccess.countTagsOfUser(user) << std::endl <<
		"  + Avarage Tags per Alboum: " << m_dataAccess.averageTagsPerAlbumOfUser(user) << std::endl;

	std::string data = "200#" + std::to_string(m_dataAccess.countAlbumsTaggedOfUser(user)) + "|";
	data += std::to_string(m_dataAccess.countTagsOfUser(user)) + "|";
	data += std::to_string((m_dataAccess.averageTagsPerAlbumOfUser(user))) + "|";

	char dataToSend[1024] = { 0 };
	strcpy(dataToSend, data.c_str());

	m_pipeControler->sendMesege(dataToSend);

}


// ******************* Queries ******************* 
void AlbumManager::topTaggedUser()
{
	std::string data = "200#";
	char dataToSend[1024] = { 0 };

	const User& user = m_dataAccess.getTopTaggedUser();

	std::cout << "The top tagged user is: " << user.getName() << std::endl;

	data += std::to_string(user.getId()) + "|";
	data += user.getName() + "|";

	strcpy(dataToSend, data.c_str());
	m_pipeControler->sendMesege(dataToSend);

}

void AlbumManager::topTaggedPicture()
{
	std::string data = "200#";
	char dataToSend[1024] = { 0 };

	const Picture& picture = m_dataAccess.getTopTaggedPicture();

	data += std::to_string(picture.getId()) + "|";
	data += picture.getName() + "|";
	data += picture.getPath() + "|";
	data += picture.getCreationDate() + "|";

	std::cout << "The top tagged picture is: " << picture.getName() << std::endl;
	strcpy(dataToSend, data.c_str());
	m_pipeControler->sendMesege(dataToSend);
}

void AlbumManager::picturesTaggedUser()
{
	std::vector<std::string> parameters = getParameterFromMesege();

	std::string data = "200#";
	char dataToSend[1024] = { 0 };

	std::string userIdStr = parameters[0];
	int userId = std::stoi(userIdStr);
	if ( !m_dataAccess.doesUserExists(userId) ) {
		m_pipeControler->sendMesege("100#Error: There is no user with this id");
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	}

	auto user = m_dataAccess.getUser(userId);

	auto taggedPictures = m_dataAccess.getTaggedPicturesOfUser(user);

	std::cout << "List of pictures that User@" << user.getId() << " tagged :" << std::endl;
	for (const Picture& picture: taggedPictures) {
		std::cout <<"   + "<< picture << std::endl;

		data += std::to_string(picture.getId()) + "|";
		data += picture.getName() + "|";
		data += picture.getPath() + "|";
		data += picture.getCreationDate() + "|";
		data += "$";
	}
	std::cout << std::endl;

	strcpy(dataToSend, data.c_str());
	m_pipeControler->sendMesege(dataToSend);
}


// ******************* Help & exit ******************* 
void AlbumManager::exit()
{
	m_pipeControler->close();
	std::exit(EXIT_SUCCESS);
}

void AlbumManager::help()
{
	system("CLS");
	printHelp();
}

//get from _msg all the parameter by the protocol
std::vector<std::string> AlbumManager::getParameterFromMesege()
{
	std::string parameter = "";
	std::vector<std::string> parametes;

	// i start in 2 becuse from index 2 start the parameter in index
	for (int i = 2; i < _msg.size(); i++)
	{
		parameter +=  _msg[i];
		if (_msg[i + 1] == '|') // check if fhinish parameter
		{
			parametes.push_back(parameter);
			parameter = "";  
			i++;
		}
	}

	return parametes;
}

std::string AlbumManager::getInputFromConsole(const std::string& message)
{
	std::string input;
	do {
		std::cout << message;
		std::getline(std::cin, input);
	} while (input.empty());
	
	return input;
}

bool AlbumManager::fileExistsOnDisk(const std::string& filename)
{
	struct stat buffer;   
	return (stat(filename.c_str(), &buffer) == 0); 
}

void AlbumManager::refreshOpenAlbum() {
	if (!isCurrentAlbumSet()) {
		throw AlbumNotOpenException();
	}
	if (m_openAlbum.getId() != -1)
	{
		m_openAlbum = m_dataAccess.openAlbum(m_currentAlbumName, m_openAlbum.getOwnerId());
	}
}

bool AlbumManager::isCurrentAlbumSet() const
{
    return !m_currentAlbumName.empty();
}

const std::vector<struct CommandGroup> AlbumManager::m_prompts  = {
	{
		"Supported Albums Operations:\n----------------------------",
		{
			{ CREATE_ALBUM        , "Create album" },
			{ OPEN_ALBUM          , "Open album" },
			{ CLOSE_ALBUM         , "Close album" },
			{ DELETE_ALBUM        , "Delete album" },
			{ LIST_ALBUMS         , "List albums" },
			{ LIST_ALBUMS_OF_USER , "List albums of user" }
		}
	},
	{
		"Supported Album commands (when specific album is open):",
		{
			{ ADD_PICTURE    , "Add picture." },
			{ REMOVE_PICTURE , "Remove picture." },
			{ SHOW_PICTURE   , "Show picture." },
			{ LIST_PICTURES  , "List pictures." },
			{ TAG_USER		 , "Tag user." },
			{ UNTAG_USER	 , "Untag user." },
			{ LIST_TAGS		 , "List tags." },
			{CHANGE_PICTURE_NAME,  "change picture name"},	
		}
	},
	{
		"Supported Users commands: ",
		{
			{ ADD_USER         , "Add user." },
			{ REMOVE_USER      , "Remove user." },
			{ LIST_OF_USER     , "List of users." },
			{ USER_STATISTICS  , "User statistics." },
		}
	},
	{
		"Supported Queries:",
		{
			{ TOP_TAGGED_USER      , "Top tagged user." },
			{ TOP_TAGGED_PICTURE   , "Top tagged picture." },
			{ PICTURES_TAGGED_USER , "Pictures tagged user." },
		}
	},
	{
		"Supported Operations:",
		{
			{ HELP , "Help (clean screen)" },
			{ EXIT , "Exit." },
		}
	}
};

const std::map<CommandType, AlbumManager::handler_func_t> AlbumManager::m_commands = {
	{ CREATE_ALBUM, &AlbumManager::createAlbum },
	{ OPEN_ALBUM, &AlbumManager::openAlbum },
	{ CLOSE_ALBUM, &AlbumManager::closeAlbum },
	{ DELETE_ALBUM, &AlbumManager::deleteAlbum },
	{ LIST_ALBUMS, &AlbumManager::listAlbums },
	{ LIST_ALBUMS_OF_USER, &AlbumManager::listAlbumsOfUser },
	{ ADD_PICTURE, &AlbumManager::addPictureToAlbum },
	{ REMOVE_PICTURE, &AlbumManager::removePictureFromAlbum },
	{ LIST_PICTURES, &AlbumManager::listPicturesInAlbum },
	{ SHOW_PICTURE, &AlbumManager::showPicture },
	{ TAG_USER, &AlbumManager::tagUserInPicture, },
	{ UNTAG_USER, &AlbumManager::untagUserInPicture },
	{ LIST_TAGS, &AlbumManager::listUserTags },
	{ CHANGE_PICTURE_NAME, &AlbumManager::changePicture},
	{ ADD_USER, &AlbumManager::addUser },
	{ REMOVE_USER, &AlbumManager::removeUser },
	{ LIST_OF_USER, &AlbumManager::listUsers },
	{ USER_STATISTICS, &AlbumManager::userStatistics },
	{ TOP_TAGGED_USER, &AlbumManager::topTaggedUser },
	{ TOP_TAGGED_PICTURE, &AlbumManager::topTaggedPicture },
	{ PICTURES_TAGGED_USER, &AlbumManager::picturesTaggedUser },
	{ HELP, &AlbumManager::help },
	{ EXIT, &AlbumManager::exit }
};
