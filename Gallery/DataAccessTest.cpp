#include "DataAccessTest.h"

DataAccessTest::DataAccessTest(DatabaseAccess& db) :
	_db(db)
{
	// Left empty
	_db.close();
	db.open();

	// get last id after open
	m_nextUserId = _db.getLastUserId();
	
	m_nextPictureId = _db.getLastPictureId();
}


// test all the check
void DataAccessTest::Testing()
{
	addingTask();
	updateingTask();
	deleteTesting();

	_db.close();

	system("pause");
}

/*
the function answer for tesing 1(add user, album, picture, tags)
input: nane
.
output: nane
.
*/

void DataAccessTest::addingTask()
{
	//ading 3 users
	std::vector<User> users;
	std::vector<Album> albums;
	std::vector<Picture> pictures;

	users.push_back(User(++m_nextUserId, "aaaa"));
	users.push_back(User(++m_nextUserId, "lior"));
	users.push_back(User(++m_nextUserId, "david"));

	for (int i = 0; i < users.size(); i++)
		_db.createUser(users[i]);

	//adding album for every user
	for (int i = 0; i < users.size(); i++)
	{
		albums.push_back(Album(users[i].getId(), "album by: " + users[i].getName()));
		_db.createAlbum(albums[i]);
		for (auto album : _db.getAlbumsOfUser(users[i])) // get white album id
		{
			albums[i] = album;
		}
	}

	int pictureIndex = 0;
	// adding pictures
	for (int i = 0; i < albums.size(); i++)
	{
		for (int j = 0; j < 2; j++)  // add 2 picture per album
		{
			pictures.push_back(Picture(++m_nextPictureId, std::to_string(j + 1) + ". " + users[i].getName()));
			_db.addPictureToAlbumByName(albums[i].getName(), pictures[pictureIndex], albums[i].getId());

			pictureIndex++;
		}
	}
	

	int albumIndex = 0;
	// add tags for pictures
	for (int i = 0; i < pictures.size(); ++i)
	{
		for (int j = 0; j < 2; j++)  // add 2 picture per album
		{
			_db.tagUserInPicture(albums[albumIndex].getId(), pictures[i].getName(), j);  // J for tags users
		}
		if (((i + 1) % 2) == 0)
			albumIndex++;
		
	}

	std::cout << "Adding task success" << std::endl;
}

//check updtae the sql db
void DataAccessTest::updateingTask()
{
	Album albumOfUser;
	User user = _db.getUser(2);
	_db.createAlbum(Album(2, "bla"));
	
	for (auto album : _db.getAlbumsOfUser(user))
		if (album.getName() == "bla")
			albumOfUser = album;
	_db.addPictureToAlbumByName("bla", Picture(++m_nextPictureId, "a"), albumOfUser.getId());
	_db.changeNameOfPicture(albumOfUser.getId(), "a", "b");

	std::cout << "Updateing task success" << std::endl;
}

//check delete the sql db
void DataAccessTest::deleteTesting()
{
	int userId = m_nextUserId - 1;

	const User& user = _db.getUser(userId);
	const std::list<Album>& albums = _db.getAlbumsOfUser(user);

	if (albums.size() > 0)  // check if have album
		for (auto album : albums) // pass in all album and delete hem
			_db.deleteAlbum(album.getName(), userId);

	_db.deleteUser(user);
	std::cout << "User @" << userId << " deleted successfully." << std::endl;
}
