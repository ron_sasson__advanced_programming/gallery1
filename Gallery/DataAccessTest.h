#pragma once
#include "DatabaseAccess.h"

class DataAccessTest
{
public:
	DataAccessTest(DatabaseAccess& db);

	void Testing();

	void addingTask();
	void updateingTask();
	void deleteTesting();

private:
	DatabaseAccess& _db;

	int m_nextUserId;

	int m_nextPictureId;
};

