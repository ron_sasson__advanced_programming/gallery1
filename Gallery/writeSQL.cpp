#include "writeSQL.h"


// when open the data base to get information and input in the varibel
void writeSQL::openUsers(sqlite3* _db, void* dbClass)
{
	int res;
	std::string sqlStatement;

	sqlStatement = "SELECT * FROM USERS;";

	char* errMessage = nullptr;

	res = sqlite3_exec(_db, sqlStatement.c_str(), callback_getUsersInfo, dbClass, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "wormg input  | " << errMessage << std::endl;
		system("pause");
		throw "wormg input";
	}
}

// get the information on user input hem on vector of user
int writeSQL::callback_getUsersInfo(void* data, int argc, char** argv, char** azColName)
{
	User user(1, ""); // this data change 
	DatabaseAccess* thisClass = (DatabaseAccess*)data;

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID") {
			user.setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME") {
			user.setName(argv[i]);
		}

		try
		{
			thisClass->addUser(user);
		}
		catch (const std::exception&)
		{
		} // ignore from this functin the exception

	}
	return 0;
}

// get from db the albums of user
std::list<Album> writeSQL::openAlbumsDB(int userId, sqlite3* _db)
{
	int res;
	std::string sqlStatement = "SELECT * FROM ALBUMS WHERE USER_ID = " + std::to_string(userId) + ";";

	char* errMessage = nullptr;
	std::list<Album> albumsOfUser;

	res = sqlite3_exec(_db, sqlStatement.c_str(), callback_getnAlbumsInfo, &albumsOfUser, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "wormg input  | " << errMessage << std::endl;
		system("pause");
		throw "wormg input";
	}

	std::list<Album> albumsToReturn;
	std::list<Picture> pictues;
	int index = 0;


	for (auto album : albumsOfUser)
	{
		pictues = openPictures(album.getId(), _db);
		for (auto picture : pictues)
		{
			album.addPicture(picture);
		}
		albumsToReturn.push_back(album);

		index++;
	}

	// open tag in picture

	return albumsToReturn;
}

// select comment to sql db of album table
int writeSQL::callback_getnAlbumsInfo(void* data, int argc, char** argv, char** azColName)
{
	Album album;
	std::list<Album>* userAlbums = (std::list<Album>*)data;


	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID") {
			album.setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME") {
			album.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "USER_ID") {
			album.setOwner(std::stoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "CREATION_DATE") {
			album.setCreationDate(argv[i]);
		}

		if ((i % 3) == 0 && i > 0) // 3 it the number of parameter
			userAlbums->push_back(album);

	}

	return 0;
}

// get all the picture of album and return is list
std::list<Picture> writeSQL::openPictures(int albumId, sqlite3* _db)
{
	int res;
	std::string sqlStatement = "SELECT * FROM PICTURES WHERE ALBUM_ID = " + std::to_string(albumId) + ";";

	char* errMessage = nullptr;
	std::list<Picture> pictureOfAlbum;

	res = sqlite3_exec(_db, sqlStatement.c_str(), callback_getPicturesInfo, &pictureOfAlbum, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "wormg input  | " << errMessage << std::endl;
		system("pause");
		throw "wormg input";
	}

	std::list<Picture> PicturesToReturn;
	std::set<int> usersIdTags;

	for (auto pictur : pictureOfAlbum)  // tags picture
	{
		usersIdTags = openTags(pictur.getId(), _db);
		for (auto userId : usersIdTags)
		{
			pictur.tagUser(userId);
		}
		PicturesToReturn.push_back(pictur);
	}

	return PicturesToReturn;
}

// select comment to sql db of picture table
int writeSQL::callback_getPicturesInfo(void* data, int argc, char** argv, char** azColName)
{
	Picture picture(1, ""); // this data change 
	std::list<Picture>* albumPicture = (std::list<Picture>*)data;


	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID") {
			picture.setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME") {
			picture.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "LOCATION") {
			picture.setPath(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE") {
			picture.setCreationDate(argv[i]);
		}

		if ((i % 4) == 0 && i > 0) // 5 it the number of parameter
			albumPicture->push_back(picture);

	}

	return 0;
}

// get all users are tag in the picture id we get
std::set<int> writeSQL::openTags(int pictureId, sqlite3* _db)
{
	int res;
	std::string sqlStatement = "SELECT * FROM TAGS WHERE PICTURE_ID = " + std::to_string(pictureId) + ";";

	char* errMessage = nullptr;
	std::set<int> tagsOfPicture;

	res = sqlite3_exec(_db, sqlStatement.c_str(), callback_getTagsInfo, &tagsOfPicture, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "wormg input  | " << errMessage << std::endl;
		system("pause");
		throw "wormg input";
	}

	return tagsOfPicture;
}

// select comment to sql db of tags table
int writeSQL::callback_getTagsInfo(void* data, int argc, char** argv, char** azColName)
{
	std::set<int>* userTags = (std::set<int>*)data;


	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "USER_ID") {
			userTags->insert(atoi(argv[i]));
		}
	}

	return 0;
}

// get user id where the user was tag there
std::list<int> writeSQL::getPictureIdByUserId(sqlite3* db, int userId)
{
	int res;
	std::string sqlStatement = "SELECT * FROM TAGS WHERE USER_ID = " + std::to_string(userId) + ";";

	char* errMessage = nullptr;
	std::list<int> tagsOfPicture;

	res = sqlite3_exec(db, sqlStatement.c_str(), callback_getTagsAbutPictureId, &tagsOfPicture, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "wormg input  | " << errMessage << std::endl;
		system("pause");
		throw "wormg input";
	}

	return tagsOfPicture;
}

int writeSQL::callback_getTagsAbutPictureId(void* data, int argc, char** argv, char** azColName)
{
	std::list<int>* userTags = (std::list<int>*)data;


	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "PICTURE_ID") {
			userTags->push_back(atoi(argv[i]));
		}
	}

	return 0;
}

/*
the function input the mesege into the sql db
input: comment of sql (string)
.
output: nane
.
*/

void writeSQL::writeCommantToDB(std::string comment, sqlite3* db)
{
	int res;
	std::string sqlStatement;

	sqlStatement = comment;

	char* errMessage = nullptr;

	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "wormg input  | " << errMessage << std::endl;
		system("pause");
	}
}

/*
the function ask from sql db the higher id 
input:db for ask
.
output: the number of max int
.
*/

int writeSQL::getLastPictureId(sqlite3* db)
{
	int res, lastId;
	std::string sqlStatement;

	sqlStatement = "SELECT max(ID) AS maxId from PICTURES";

	char* errMessage = nullptr;
	
	res = sqlite3_exec(db, sqlStatement.c_str(), callback_getLastPictureId, &lastId, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "wormg input  | " << errMessage << std::endl;
		system("pause");
	}

	return lastId;
}

// get from select comment the max id
int writeSQL::callback_getLastPictureId(void* data, int argc, char** argv, char** azColName)
{
	int* maxId = (int*)data;
	
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "maxId") 
		{
			*maxId = atoi(argv[i]);
		}
	}

	return 0;
}

/*
the function get picture by id
input:the db, picture id list white all the id
.
output: list of Picture
.
*/

std::list<Picture> writeSQL::getPictureBayId(sqlite3* db, std::list<int> picturesId)
{
	std::list<Picture> PicturesToReturn;
	if (picturesId.size() > 0)
	{
		int res;
		std::string sqlStatement = "SELECT * FROM PICTURES WHERE ID in (";
		for (auto picturId : picturesId)
		{
			sqlStatement += std::to_string(picturId) + ", ";
		}

		sqlStatement[sqlStatement.size() - 2] = ')';
		sqlStatement += ";";

		char* errMessage = nullptr;
		std::list<Picture> pictureOfAlbum;

		res = sqlite3_exec(db, sqlStatement.c_str(), callback_getPicturesInfo, &pictureOfAlbum, &errMessage);
		if (res != SQLITE_OK)
		{
			std::cout << "wormg input  | " << errMessage << std::endl;
			system("pause");
			throw "wormg input";
		}
		
		std::set<int> usersIdTags;

		for (auto pictur : pictureOfAlbum)  // tags picture
		{
			usersIdTags = openTags(pictur.getId(), db);
			for (auto userId : usersIdTags)
			{
				pictur.tagUser(userId);
			}
			PicturesToReturn.push_back(pictur);
		}
	}
	return PicturesToReturn;
}

//return the user id white the max tags
int writeSQL::getTopUserTagged(sqlite3* db)
{
	int res;
	std::set<int> userId;
	std::string sqlStatement;

	sqlStatement = "SELECT USER_ID, max(TAGS.USER_ID = USERS.ID) FROM TAGS JOIN USERS; ";
		
	char* errMessage = nullptr;

	res = sqlite3_exec(db, sqlStatement.c_str(), callback_getTagsInfo, &userId, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "wormg input  | " << errMessage << std::endl;
		system("pause");
	}

	int userWhiteMaxTags;
	for (auto maxId : userId)
	{
		userWhiteMaxTags = maxId;
		break;
	}
	
	return userWhiteMaxTags;
}

//callback_getTagsAbutPictureId

int writeSQL::getTopAlbumTagged(sqlite3* db)
{
	int res;
	std::list<int> PictureId;
	std::string sqlStatement;

	sqlStatement = "SELECT PICTURE_ID, max(TAGS.PICTURE_ID = PICTURES.ID) FROM TAGS JOIN PICTURES;";

	char* errMessage = nullptr;

	res = sqlite3_exec(db, sqlStatement.c_str(), callback_getTagsAbutPictureId, &PictureId, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "wormg input  | " << errMessage << std::endl;
		system("pause");
	}

	
	for (auto id : PictureId)
	{
		return id;
	}
}