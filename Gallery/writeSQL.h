#pragma once
#include "sqlite3.h"
#include "IDataAccess.h"
#include"DatabaseAccess.h"
#include <io.h>
#include <iostream>
#include <vector>

class writeSQL
{
public:

	writeSQL() = default;

	// when open the data base to get information and input in the varibel
	//user
	static void openUsers(sqlite3* db, void* dbClass);
	static int callback_getUsersInfo(void* data, int argc, char** argv, char** azColName);

	//album
	static std::list<Album> openAlbumsDB(int userId, sqlite3* db);
	static int callback_getnAlbumsInfo(void* data, int argc, char** argv, char** azColName);

	//picture
	static std::list<Picture> openPictures(int albumId, sqlite3* db);
	static int callback_getPicturesInfo(void* data, int argc, char** argv, char** azColName);

	static int getLastPictureId(sqlite3* db);
	static int callback_getLastPictureId(void* data, int argc, char** argv, char** azColName);

	static std::list<Picture> getPictureBayId(sqlite3* db, std::list<int> picturesId);


	//tag
	static std::set<int> openTags(int pictureId, sqlite3* db);
	static int callback_getTagsInfo(void* data, int argc, char** argv, char** azColName);

	static std::list<int> getPictureIdByUserId(sqlite3* db, int userId);
	static int callback_getTagsAbutPictureId(void* data, int argc, char** argv, char** azColName);

	static void writeCommantToDB(std::string comment, sqlite3* db);

	//Queries
	static int getTopUserTagged(sqlite3* db);
	static int getTopAlbumTagged(sqlite3* db);
	static int callback_getId(void* data, int argc, char** argv, char** azColName);


};

