#include "OpenDraw.h"


OpenDraw::OpenDraw()
{
}

OpenDraw::~OpenDraw()
{
	_processInfo = nullptr;
}


/*
the function open picture in aplican the user chose
input:the path of the picture
.
output: nane (open the picture)
.
*/


void OpenDraw::openDrowing(std::string PathPicture)
{
	system("CLS");
	printManu();
	//int userChoice = getNumberFromUser();
	
	/*if (userChoice == PAINT_CHOICE)*/
	openApp(PathPicture, "C:\\Windows\\System32\\mspaint.exe");

	/*else if (userChoice == IRFAN_VIEW_CHOICE)
		openApp(PathPicture, "IrfanView\\i_view64.exe");*/
	
}

/*
the function open the picture in paint app
input: the path of picture
.
output: nane
.
*/

void OpenDraw::openApp(std::string PathPicture, std::string appName)
{

	LPCSTR appNmae = appName.c_str();
	STARTUPINFO startupInfo;
	PROCESS_INFORMATION processInfo;
	size_t outSize;


	memset(&startupInfo, 0, sizeof(startupInfo));
	memset(&processInfo, 0, sizeof(processInfo));

	std::string appAndPictur = appName + " " + PathPicture;

	_processInfo = &processInfo;

	CreateProcess(0, LPSTR(appAndPictur.c_str()), 0, 0, 0, 0, 0, 0, &startupInfo, &processInfo);


	HANDLE cehckChange = FindFirstChangeNotification(  // for check if process change
		LPSTR(PathPicture.substr(0, PathPicture.find_last_of("/\\")).c_str()),
		TRUE,
		FILE_NOTIFY_CHANGE_FILE_NAME |
		FILE_NOTIFY_CHANGE_DIR_NAME |
		FILE_NOTIFY_CHANGE_ATTRIBUTES |
		FILE_NOTIFY_CHANGE_SIZE |
		FILE_NOTIFY_CHANGE_LAST_WRITE |
		FILE_NOTIFY_CHANGE_SECURITY
	);

	checkChangeOnPicture(processInfo, cehckChange);
		
	WaitForSingleObject(processInfo.hProcess, INFINITE);

	CloseHandle(processInfo.hProcess);


}


void OpenDraw::closeProcess()
{
	TerminateProcess(_processInfo->hProcess, 9);
	CloseHandle(_processInfo->hProcess);
}


void OpenDraw::printManu()
{
	std::cout << "application for pictur :)" << std::endl;
	std::cout << "-------------------------" << std::endl;
	std::cout << "1. paint app" << std::endl;
	std::cout << "2. Irfan View" << std::endl;
}

int OpenDraw::getNumberFromUser()
{
	std::string message("\nPlease enter any command(use number): ");
	std::string numericStr("12");

	std::cout << message << std::endl;
	std::string input;
	std::getline(std::cin, input);

	while (std::cin.fail() || std::cin.eof() || input.find_first_not_of(numericStr) != std::string::npos) {

		std::cout << "Please enter a number 1 or 2 only" << std::endl;

		if (input.find_first_not_of(numericStr) == std::string::npos) {
			std::cin.clear();
		}

		std::cout << std::endl << message << std::endl;
		std::getline(std::cin, input);
	}

	return std::atoi(input.c_str());
}

/*
the function check if been change on picture file
input:the proces who open and whon to chagne, handle of check path of the proces
.
output: nane
.
*/

void OpenDraw::checkChangeOnPicture(PROCESS_INFORMATION processInfo, HANDLE hNotify)
{
	//https://docs.microsoft.com/en-us/windows/win32/fileio/obtaining-directory-change-notifications
	DWORD dwWaitStatus;

	HANDLE handels[] = { hNotify, processInfo.hProcess };
	dwWaitStatus = WaitForMultipleObjects(2, handels, FALSE, INFINITE);

	switch (dwWaitStatus)
	{
	case WAIT_OBJECT_0:
		if (FindNextChangeNotification(hNotify) == FALSE)
			throw "Error: " + std::to_string(GetLastError());
		else
			std::cout << std::endl << "XXX the file as change XXX" << std::endl;
		break;
	default:
		std::cout << std::endl << "XXX the file as not change XXX" << std::endl;
		break;
	}
}

