#pragma once
#include "sqlite3.h"
#include "IDataAccess.h"
#include <io.h>
#include <iostream>
#include <vector>
#include "writeSQL.h"
#include "OpenDraw.h"

class DatabaseAccess : public IDataAccess
{
public:
	// album related
	const std::list<Album> getAlbums();
	const std::list<Album> getAlbumsOfUser(const User& user);
	void createAlbum(const Album& album);
	void deleteAlbum(const std::string& albumName, int userId);
	bool doesAlbumExists(const std::string& albumName, int userId);
	Album openAlbum(const std::string& albumName, int userId);
	void closeAlbum(Album& pAlbum);
	void printAlbums();

	// picture related
	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture, int albumId);
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName, int albumId);
	void tagUserInPicture(const int albumId, const std::string& pictureName, int userId);
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId);
	int getLastPictureId();
	void changeNameOfPicture(const int albumId, const std::string& pictureName, const std::string nameToChange);

	// user related
	void printUsers();
	std::vector <User> getUsers();
	User getUser(int userId);
	void createUser(User& user);
	void deleteUser(const User& user);
	bool doesUserExists(int userId);
	int getLastUserId();
	void addUser(User user);


	// user statistics
	int countAlbumsOwnedOfUser(const User& user);
	int countAlbumsTaggedOfUser(const User& user);
	int countTagsOfUser(const User& user);
	float averageTagsPerAlbumOfUser(const User& user);

	// queries
	User getTopTaggedUser();
	Picture getTopTaggedPicture();
	std::list<Picture> getTaggedPicturesOfUser(const User& user);

	// Basic
	bool open();
	void close();
	void clear();

	//app paint
	void openApp(std::string pathPucture);
	void killApp();

private:
	sqlite3* _db;
	std::vector<User> m_users;
	OpenDraw* draw;

	int getLastIdOFAlbum();

	bool isUserTagInThisAlbum(Album album, std::list<Picture> pictureUserTag);
};

