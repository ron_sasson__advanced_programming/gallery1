﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace GallaryApp
{
    public partial class Form1 : Form
    {
        private PipeMengmer mengmer;

        public Form1()
        {
            InitializeComponent();
            Thread t = new Thread(new ThreadStart(openCPPapp));
            t.Start();
            pipe p = new pipe();
            mengmer = new PipeMengmer(p);
        }

        private static void openCPPapp()
        {
            Thread.Sleep(5);
            System.Diagnostics.Process.Start("Gallery.exe");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            sidePanel.Height = home.Height;
            sidePanel.Top = home.Top;
            homeCustomConroler1.BringToFront();

            userControl11.SetMengmer(mengmer);
            userControl11.setListItem(listCustomControler1);

            albumCustomControler1.SetMengmer(mengmer);
            albumCustomControler1.setListItem(listCustomControler1);

            tagCustomControler1.SetMengmer(mengmer);
            tagCustomControler1.setListItem(listCustomControler1);

            statisticsCustomControl1.SetMengmer(mengmer);
            statisticsCustomControl1.setListItem(listCustomControler1);

            pictureCustomControl1.SetMengmer(mengmer);
            pictureCustomControl1.setListItem(listCustomControler1);        
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            mengmer.exit();
            this.Close();
        }

        private void home_Click(object sender, EventArgs e)
        {
            sidePanel.Height = home.Height;
            sidePanel.Top = home.Top;

            homeCustomConroler1.BringToFront();
        }

        private void Albums_Click(object sender, EventArgs e)
        {
            sidePanel.Height = Albums.Height;
            sidePanel.Top = Albums.Top;

            albumCustomControler1.BringToFront();
        }

        private void Users_Click(object sender, EventArgs e)
        {
            sidePanel.Height = Users.Height;
            sidePanel.Top = Users.Top;

            userControl11.BringToFront();  
        }

        private void Tags_Click(object sender, EventArgs e)
        {
            if (mengmer.checkAlbum)  // check if album is open
            {
                sidePanel.Height = Tags.Height;
                sidePanel.Top = Tags.Top;

                tagCustomControler1.BringToFront();
            }
            else
            {
                MessageBox.Show("you need to open album for this feature\nfor open album:\nAlbums -> open album", 
                    "Erorr:no have open album");
            }
}

        private void Statistics_Click(object sender, EventArgs e)
        {
            sidePanel.Height = Statistics.Height;
            sidePanel.Top = Statistics.Top;

            statisticsCustomControl1.BringToFront();
        }

        private void pict_Click(object sender, EventArgs e)
        {
            if (mengmer.checkAlbum)  // check if album is open
            {
                sidePanel.Height = pict.Height;
                sidePanel.Top = pict.Top;

                pictureCustomControl1.BringToFront();
            }
            else
            {
                MessageBox.Show("you need to open album for this feature\nfor open album:\nAlbums -> open album",
                    "Erorr:no have open album");
            }
        }


    }
}
