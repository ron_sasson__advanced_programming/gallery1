﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GallaryApp
{
    public partial class listViewCustomContoroler : UserControl
    {
        public listViewCustomContoroler()
        {
            InitializeComponent();
        }

        #region
        private string IdString;
        private string _parameters;
        private Image _icon;



        [Category("Custom Props")]
        public string Id
        {
            get { return IdString; }
            set { IdString = value; label1.Text = value; }
        }

        [Category("Custom Props")]
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; label2.Text = value; }
        }

        [Category("Custom Props")]
        public Image Icon
        {
            get { return _icon; }
            set { _icon = value; pictureBox1.Image = value; }
        }
        #endregion

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
