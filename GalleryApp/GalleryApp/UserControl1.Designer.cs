﻿namespace GallaryApp
{
    partial class UserControl1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControl1));
            this.addUser = new System.Windows.Forms.Button();
            this.addUserTextBox = new System.Windows.Forms.TextBox();
            this.RemoveUser = new System.Windows.Forms.Button();
            this.listUsers = new System.Windows.Forms.Button();
            this.statistic = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.RemoveUserTextBox = new System.Windows.Forms.TextBox();
            this.statisticUserTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // addUser
            // 
            this.addUser.BackColor = System.Drawing.Color.White;
            this.addUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.addUser.ForeColor = System.Drawing.Color.Black;
            this.addUser.Location = new System.Drawing.Point(45, 100);
            this.addUser.Name = "addUser";
            this.addUser.Size = new System.Drawing.Size(116, 37);
            this.addUser.TabIndex = 0;
            this.addUser.Text = "Adding user";
            this.addUser.UseVisualStyleBackColor = false;
            this.addUser.Click += new System.EventHandler(this.addUser_Click);
            // 
            // addUserTextBox
            // 
            this.addUserTextBox.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.addUserTextBox.ForeColor = System.Drawing.Color.Black;
            this.addUserTextBox.Location = new System.Drawing.Point(103, 405);
            this.addUserTextBox.Name = "addUserTextBox";
            this.addUserTextBox.Size = new System.Drawing.Size(110, 22);
            this.addUserTextBox.TabIndex = 1;
            this.addUserTextBox.TextChanged += new System.EventHandler(this.addUserTextBox_TextChanged);
            // 
            // RemoveUser
            // 
            this.RemoveUser.BackColor = System.Drawing.Color.White;
            this.RemoveUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.RemoveUser.ForeColor = System.Drawing.Color.Black;
            this.RemoveUser.Location = new System.Drawing.Point(284, 101);
            this.RemoveUser.Name = "RemoveUser";
            this.RemoveUser.Size = new System.Drawing.Size(116, 37);
            this.RemoveUser.TabIndex = 3;
            this.RemoveUser.Text = "Remove user";
            this.RemoveUser.UseVisualStyleBackColor = false;
            this.RemoveUser.Click += new System.EventHandler(this.RemoveUser_Click);
            // 
            // listUsers
            // 
            this.listUsers.BackColor = System.Drawing.Color.White;
            this.listUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.listUsers.ForeColor = System.Drawing.Color.Black;
            this.listUsers.Location = new System.Drawing.Point(527, 101);
            this.listUsers.Name = "listUsers";
            this.listUsers.Size = new System.Drawing.Size(116, 37);
            this.listUsers.TabIndex = 4;
            this.listUsers.Text = "UserList";
            this.listUsers.UseVisualStyleBackColor = false;
            this.listUsers.Click += new System.EventHandler(this.listUsers_Click);
            // 
            // statistic
            // 
            this.statistic.BackColor = System.Drawing.Color.White;
            this.statistic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.statistic.ForeColor = System.Drawing.Color.Black;
            this.statistic.Location = new System.Drawing.Point(770, 101);
            this.statistic.Name = "statistic";
            this.statistic.Size = new System.Drawing.Size(116, 37);
            this.statistic.TabIndex = 5;
            this.statistic.Text = "Statistics";
            this.statistic.UseVisualStyleBackColor = false;
            this.statistic.Click += new System.EventHandler(this.statistic_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(-1, 405);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "user name:";
            // 
            // RemoveUserTextBox
            // 
            this.RemoveUserTextBox.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.RemoveUserTextBox.ForeColor = System.Drawing.Color.Black;
            this.RemoveUserTextBox.Location = new System.Drawing.Point(345, 407);
            this.RemoveUserTextBox.Name = "RemoveUserTextBox";
            this.RemoveUserTextBox.Size = new System.Drawing.Size(110, 22);
            this.RemoveUserTextBox.TabIndex = 10;
            // 
            // statisticUserTextBox
            // 
            this.statisticUserTextBox.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.statisticUserTextBox.ForeColor = System.Drawing.Color.Black;
            this.statisticUserTextBox.Location = new System.Drawing.Point(816, 409);
            this.statisticUserTextBox.Name = "statisticUserTextBox";
            this.statisticUserTextBox.Size = new System.Drawing.Size(110, 22);
            this.statisticUserTextBox.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(241, 407);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 20);
            this.label7.TabIndex = 20;
            this.label7.Text = "user name:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(712, 409);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 20);
            this.label8.TabIndex = 21;
            this.label8.Text = "user name:";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.InitialImage = null;
            this.pictureBox3.Location = new System.Drawing.Point(716, 54);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(210, 399);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 22;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.InitialImage = null;
            this.pictureBox4.Location = new System.Drawing.Point(245, 51);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(210, 399);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 17;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.InitialImage = null;
            this.pictureBox2.Location = new System.Drawing.Point(477, 51);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(210, 399);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(3, 51);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(210, 399);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // UserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.Controls.Add(this.statistic);
            this.Controls.Add(this.listUsers);
            this.Controls.Add(this.addUser);
            this.Controls.Add(this.RemoveUser);
            this.Controls.Add(this.statisticUserTextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.RemoveUserTextBox);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addUserTextBox);
            this.Controls.Add(this.pictureBox1);
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size(978, 482);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox addUserTextBox;
        public System.Windows.Forms.Button RemoveUser;
        public System.Windows.Forms.Button listUsers;
        public System.Windows.Forms.Button statistic;
        public System.Windows.Forms.Button addUser;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RemoveUserTextBox;
        private System.Windows.Forms.TextBox statisticUserTextBox;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}
