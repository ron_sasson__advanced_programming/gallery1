﻿using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Pipes;
using System.IO;
using System.Threading;

namespace GallaryApp
{
    public class pipe
    {
        NamedPipeServerStream pipeServer;
        private Stream ioStream;


        public pipe()
        {
            pipeServer =
                new NamedPipeServerStream("Gallery", PipeDirection.InOut, 1);
            ioStream = pipeServer;

        }

        public bool connect()
        {

            // Wait for a client to connect
            pipeServer.WaitForConnection();

            return pipeServer.IsConnected;
        }

        public bool isConnected()
        {
            return pipeServer.IsConnected;
        }

        public string getMesgeFromClient()
        {  
            if (!isConnected())
                return "";
 
            byte[] inBuffer = new byte[1024];
            ioStream.Read(inBuffer, 0, 1024);
    

            string res =  Encoding.ASCII.GetString(inBuffer).TrimEnd((char)0);

            System.Console.WriteLine("code from engine " + res);
            
            return res;
        }

        public void mesegeToClient(string msg)
        {
            if (!isConnected())
                return;

            byte[] t = Encoding.ASCII.GetBytes(msg);
            byte[] inBuffer = new byte[t.Length + 1];

            for (int i = 0; i < t.Length; i++)
            {
                inBuffer[i] = t[i];
            }
            inBuffer[inBuffer.Length - 1] = 0;

            try
            {
                ioStream.Write(inBuffer, 0, inBuffer.Length);

                ioStream.Flush();
            }
            catch
            {

            }
        }

        public void close()
        {
            pipeServer.Close();
        }
    }
}
