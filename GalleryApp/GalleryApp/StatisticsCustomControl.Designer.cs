﻿namespace GallaryApp
{
    partial class StatisticsCustomControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatisticsCustomControl));
            this.topTaggedUser = new System.Windows.Forms.Button();
            this.picturesTaggedUser = new System.Windows.Forms.Button();
            this.topTaggedPicture = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.userNameTextBox = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // topTaggedUser
            // 
            this.topTaggedUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.topTaggedUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.topTaggedUser.ForeColor = System.Drawing.Color.Black;
            this.topTaggedUser.Location = new System.Drawing.Point(92, 75);
            this.topTaggedUser.Name = "topTaggedUser";
            this.topTaggedUser.Size = new System.Drawing.Size(135, 60);
            this.topTaggedUser.TabIndex = 5;
            this.topTaggedUser.Text = "Top Tagged User";
            this.topTaggedUser.UseVisualStyleBackColor = true;
            this.topTaggedUser.Click += new System.EventHandler(this.topTaggedUser_Click);
            // 
            // picturesTaggedUser
            // 
            this.picturesTaggedUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.picturesTaggedUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.picturesTaggedUser.ForeColor = System.Drawing.Color.Black;
            this.picturesTaggedUser.Location = new System.Drawing.Point(759, 75);
            this.picturesTaggedUser.Name = "picturesTaggedUser";
            this.picturesTaggedUser.Size = new System.Drawing.Size(135, 60);
            this.picturesTaggedUser.TabIndex = 6;
            this.picturesTaggedUser.Text = "Pictures Tagged User";
            this.picturesTaggedUser.UseVisualStyleBackColor = true;
            this.picturesTaggedUser.Click += new System.EventHandler(this.picturesTaggedUser_Click);
            // 
            // topTaggedPicture
            // 
            this.topTaggedPicture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.topTaggedPicture.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.topTaggedPicture.ForeColor = System.Drawing.Color.Black;
            this.topTaggedPicture.Location = new System.Drawing.Point(422, 75);
            this.topTaggedPicture.Name = "topTaggedPicture";
            this.topTaggedPicture.Size = new System.Drawing.Size(135, 60);
            this.topTaggedPicture.TabIndex = 7;
            this.topTaggedPicture.Text = "Top Tagged Picture";
            this.topTaggedPicture.UseVisualStyleBackColor = true;
            this.topTaggedPicture.Click += new System.EventHandler(this.topTaggedPicture_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(691, 386);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 20);
            this.label8.TabIndex = 22;
            this.label8.Text = "user name:";
            // 
            // userNameTextBox
            // 
            this.userNameTextBox.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.userNameTextBox.ForeColor = System.Drawing.Color.Black;
            this.userNameTextBox.Location = new System.Drawing.Point(815, 386);
            this.userNameTextBox.Name = "userNameTextBox";
            this.userNameTextBox.Size = new System.Drawing.Size(124, 22);
            this.userNameTextBox.TabIndex = 23;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(29, 44);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(280, 394);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(357, 44);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(280, 394);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 25;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(675, 44);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(280, 394);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 26;
            this.pictureBox3.TabStop = false;
            // 
            // StatisticsCustomControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.userNameTextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.topTaggedPicture);
            this.Controls.Add(this.picturesTaggedUser);
            this.Controls.Add(this.topTaggedUser);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Name = "StatisticsCustomControl";
            this.Size = new System.Drawing.Size(978, 482);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button topTaggedUser;
        private System.Windows.Forms.Button picturesTaggedUser;
        private System.Windows.Forms.Button topTaggedPicture;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox userNameTextBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}
