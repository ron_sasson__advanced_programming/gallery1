﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GallaryApp
{
    public partial class StatisticsCustomControl : UserControl
    {
        private PipeMengmer mengmer;
        private listCustomControler listItem;

        public StatisticsCustomControl()
        {
            InitializeComponent();
        }

        private void topTaggedUser_Click(object sender, EventArgs e)
        {
            List<List<string>> user = new List<List<string>>();
            user.Add(mengmer.topTaggedUser());

            listItem.populateItem(user, "user");
            listItem.BringToFront();
        }

        private void topTaggedPicture_Click(object sender, EventArgs e)
        {
            List<List<string>> picture = new List<List<string>>();
            picture.Add(mengmer.topTaggedPicture());

            listItem.populateItem(picture, "picture");
            listItem.BringToFront();
        }

        private void picturesTaggedUser_Click(object sender, EventArgs e)
        {
            int userId = 0;
            bool checkUserExsist = true;
            if (userNameTextBox.Text != "")
            {

                try
                {
                    userId = mengmer.getUserId(userNameTextBox.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("The user not exsist");
                    checkUserExsist = false;
                }

                if (checkUserExsist)
                {
                    try
                    {
                        listItem.populateItem(mengmer.picturesTaggedUser(userId), "picture");
                        listItem.BringToFront();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("have problem try agin");
                    }

                    userNameTextBox.Text = "";
                }

            }
        }

        public void setListItem(listCustomControler listItem)
        {
            this.listItem = listItem;
        }

        public void SetMengmer(PipeMengmer mengmer)
        {
            this.mengmer = mengmer;
        }

    }
}
