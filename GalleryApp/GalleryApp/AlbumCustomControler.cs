﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GallaryApp
{
    public partial class AlbumCustomControler : UserControl
    {
        private PipeMengmer mengmer;
        private listCustomControler listItem;

        public AlbumCustomControler()
        {
            InitializeComponent();
        }

        private void addAlbum_b_Click(object sender, EventArgs e)
        {
            int userId = 0;
            bool checkUserExsist = true;
            if (createAlbumTextBox.Text != "" && userNameAddTextBox.Text != "")
            {
                try
                {
                    userId = mengmer.getUserId(userNameAddTextBox.Text); // get user id
                }
                catch (Exception) // if user id not exsist
                {
                    MessageBox.Show("The user not exsist");
                    checkUserExsist = false;
                }

                if (checkUserExsist)
                {
                    try
                    {
                        mengmer.createAlbum(userId, createAlbumTextBox.Text);
                        MessageBox.Show("Album add successfully");
                    }
                    catch (Exception)
                    {
                        // it mean the user are axsist
                        MessageBox.Show("The album exsist");
                    }
                }

                createAlbumTextBox.Text = "";
                userNameAddTextBox.Text = "";
            }
        }

        private void removeAlbum_b_Click(object sender, EventArgs e)
        {
            int userId = 0;
            bool checkUserExsist = true;
            if (removeAlbumTextBox.Text != "" && removeUsertextBox.Text != "")
            {
                try
                {
                    userId = mengmer.getUserId(removeUsertextBox.Text); // get user id
                }
                catch (Exception) // if user id not exsist
                {
                    MessageBox.Show("The user not exsist");
                    checkUserExsist = false;
                }

                if (checkUserExsist)
                {
                    try
                    {
                        mengmer.deleteAlbum(userId, removeAlbumTextBox.Text);
                        MessageBox.Show("Album remove successfully");
                    }
                    catch (Exception)
                    {
                        // it mean the user are axsist
                        MessageBox.Show("The album not exsist");
                    }
                }

                removeAlbumTextBox.Text = "";
                removeUsertextBox.Text = "";
            }
        }

        private void openAlbum_b_Click(object sender, EventArgs e)
        {
            int userId = 0;
            bool checkUserExsist = true;
            if (albumOpenTextBox.Text != "" && openUserTextBox.Text != "")
            {
                try
                {
                    userId = mengmer.getUserId(openUserTextBox.Text); // get user id
                }
                catch (Exception) // if user id not exsist
                {
                    MessageBox.Show("The user not exsist");
                    checkUserExsist = false;
                }

                if (checkUserExsist)
                {
                    try
                    {
                        mengmer.openAlbum(userId, albumOpenTextBox.Text);
                        MessageBox.Show("Album opend successfully");
                        mengmer.checkAlbum = true;
                    }
                    catch (Exception)
                    {
                        // it mean the user are axsist
                        MessageBox.Show("The album not exsist");
                    }
                }

                albumOpenTextBox.Text = "";
                openUserTextBox.Text = "";
            }
        }

        private void closeAlbum_b_Click(object sender, EventArgs e)
        {
            if (mengmer.checkAlbum)  // can close only if it is open
            {
                mengmer.closeAlbum();
                MessageBox.Show("Album closed");
                mengmer.checkAlbum = false;
            }
            else
                MessageBox.Show("no have album is open");
        }

        private void listOfAllAlbum_b_Click(object sender, EventArgs e)
        {
            listItem.populateItem(mengmer.listAlbums(), "album");
            listItem.BringToFront();
        }

        private void listAlbumOfUser_b_Click(object sender, EventArgs e)
        {
            int userId = 0;
            if (UserListAlbumtextBox.Text != "")
            {
                try
                {
                    userId = mengmer.getUserId(UserListAlbumtextBox.Text); // get user id
                    listItem.populateItem(mengmer.listAlbumsOfUser(userId), "album");
                    listItem.BringToFront();
                }
                catch (Exception) // if user id not exsist
                {
                    MessageBox.Show("The user not exsist");
                }

                UserListAlbumtextBox.Text = "";


            }

        }

        public void setListItem(listCustomControler listItem)
        {
            this.listItem = listItem;
        }

        public void SetMengmer(PipeMengmer mengmer)
        {
            this.mengmer = mengmer;
        }
    }
}
