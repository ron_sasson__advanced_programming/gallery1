﻿namespace GallaryApp
{
    partial class AlbumCustomControler
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlbumCustomControler));
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.removeAlbum_b = new System.Windows.Forms.Button();
            this.addAlbum_b = new System.Windows.Forms.Button();
            this.listOfAllAlbum_b = new System.Windows.Forms.Button();
            this.openAlbum_b = new System.Windows.Forms.Button();
            this.closeAlbum_b = new System.Windows.Forms.Button();
            this.listAlbumOfUser_b = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.createAlbumTextBox = new System.Windows.Forms.TextBox();
            this.albumOpenTextBox = new System.Windows.Forms.TextBox();
            this.UserListAlbumtextBox = new System.Windows.Forms.TextBox();
            this.removeAlbumTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.userNameAddTextBox = new System.Windows.Forms.TextBox();
            this.removeUsertextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.openUserTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.addAlbum = new System.Windows.Forms.PictureBox();
            this.openAlbum = new System.Windows.Forms.PictureBox();
            this.listOfAllAlbum = new System.Windows.Forms.PictureBox();
            this.listAlbumOfUser = new System.Windows.Forms.PictureBox();
            this.closeAlbum = new System.Windows.Forms.PictureBox();
            this.removeAlbum = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.addAlbum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.openAlbum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listOfAllAlbum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listAlbumOfUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeAlbum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.removeAlbum)).BeginInit();
            this.SuspendLayout();
            // 
            // removeAlbum_b
            // 
            this.removeAlbum_b.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.removeAlbum_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.removeAlbum_b.ForeColor = System.Drawing.Color.Black;
            this.removeAlbum_b.Location = new System.Drawing.Point(91, 272);
            this.removeAlbum_b.Name = "removeAlbum_b";
            this.removeAlbum_b.Size = new System.Drawing.Size(124, 59);
            this.removeAlbum_b.TabIndex = 6;
            this.removeAlbum_b.Text = "Remove Album";
            this.removeAlbum_b.UseVisualStyleBackColor = true;
            this.removeAlbum_b.Click += new System.EventHandler(this.removeAlbum_b_Click);
            // 
            // addAlbum_b
            // 
            this.addAlbum_b.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addAlbum_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.addAlbum_b.ForeColor = System.Drawing.Color.Black;
            this.addAlbum_b.Location = new System.Drawing.Point(91, 32);
            this.addAlbum_b.Name = "addAlbum_b";
            this.addAlbum_b.Size = new System.Drawing.Size(110, 46);
            this.addAlbum_b.TabIndex = 7;
            this.addAlbum_b.Text = "Add Album";
            this.addAlbum_b.UseVisualStyleBackColor = true;
            this.addAlbum_b.Click += new System.EventHandler(this.addAlbum_b_Click);
            // 
            // listOfAllAlbum_b
            // 
            this.listOfAllAlbum_b.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.listOfAllAlbum_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.listOfAllAlbum_b.ForeColor = System.Drawing.Color.Black;
            this.listOfAllAlbum_b.Location = new System.Drawing.Point(683, 32);
            this.listOfAllAlbum_b.Name = "listOfAllAlbum_b";
            this.listOfAllAlbum_b.Size = new System.Drawing.Size(110, 57);
            this.listOfAllAlbum_b.TabIndex = 8;
            this.listOfAllAlbum_b.Text = "List Of All Album";
            this.listOfAllAlbum_b.UseVisualStyleBackColor = true;
            this.listOfAllAlbum_b.Click += new System.EventHandler(this.listOfAllAlbum_b_Click);
            // 
            // openAlbum_b
            // 
            this.openAlbum_b.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.openAlbum_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.openAlbum_b.ForeColor = System.Drawing.Color.Black;
            this.openAlbum_b.Location = new System.Drawing.Point(384, 32);
            this.openAlbum_b.Name = "openAlbum_b";
            this.openAlbum_b.Size = new System.Drawing.Size(110, 57);
            this.openAlbum_b.TabIndex = 9;
            this.openAlbum_b.Text = "Open Album";
            this.openAlbum_b.UseVisualStyleBackColor = true;
            this.openAlbum_b.Click += new System.EventHandler(this.openAlbum_b_Click);
            // 
            // closeAlbum_b
            // 
            this.closeAlbum_b.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.closeAlbum_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.closeAlbum_b.ForeColor = System.Drawing.Color.Black;
            this.closeAlbum_b.Location = new System.Drawing.Point(384, 278);
            this.closeAlbum_b.Name = "closeAlbum_b";
            this.closeAlbum_b.Size = new System.Drawing.Size(110, 53);
            this.closeAlbum_b.TabIndex = 10;
            this.closeAlbum_b.Text = "Close Album";
            this.closeAlbum_b.UseVisualStyleBackColor = true;
            this.closeAlbum_b.Click += new System.EventHandler(this.closeAlbum_b_Click);
            // 
            // listAlbumOfUser_b
            // 
            this.listAlbumOfUser_b.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.listAlbumOfUser_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.listAlbumOfUser_b.ForeColor = System.Drawing.Color.Black;
            this.listAlbumOfUser_b.Location = new System.Drawing.Point(683, 278);
            this.listAlbumOfUser_b.Name = "listAlbumOfUser_b";
            this.listAlbumOfUser_b.Size = new System.Drawing.Size(110, 53);
            this.listAlbumOfUser_b.TabIndex = 11;
            this.listAlbumOfUser_b.Text = "List Album Of User";
            this.listAlbumOfUser_b.UseVisualStyleBackColor = true;
            this.listAlbumOfUser_b.Click += new System.EventHandler(this.listAlbumOfUser_b_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(29, 426);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Album Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(330, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "Album Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(29, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "Album Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(618, 426);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "User Name";
            // 
            // createAlbumTextBox
            // 
            this.createAlbumTextBox.ForeColor = System.Drawing.Color.Black;
            this.createAlbumTextBox.Location = new System.Drawing.Point(140, 181);
            this.createAlbumTextBox.Name = "createAlbumTextBox";
            this.createAlbumTextBox.Size = new System.Drawing.Size(109, 22);
            this.createAlbumTextBox.TabIndex = 16;
            // 
            // albumOpenTextBox
            // 
            this.albumOpenTextBox.ForeColor = System.Drawing.Color.Black;
            this.albumOpenTextBox.Location = new System.Drawing.Point(441, 181);
            this.albumOpenTextBox.Name = "albumOpenTextBox";
            this.albumOpenTextBox.Size = new System.Drawing.Size(104, 22);
            this.albumOpenTextBox.TabIndex = 17;
            // 
            // UserListAlbumtextBox
            // 
            this.UserListAlbumtextBox.ForeColor = System.Drawing.Color.Black;
            this.UserListAlbumtextBox.Location = new System.Drawing.Point(718, 424);
            this.UserListAlbumtextBox.Name = "UserListAlbumtextBox";
            this.UserListAlbumtextBox.Size = new System.Drawing.Size(118, 22);
            this.UserListAlbumtextBox.TabIndex = 18;
            // 
            // removeAlbumTextBox
            // 
            this.removeAlbumTextBox.ForeColor = System.Drawing.Color.Black;
            this.removeAlbumTextBox.Location = new System.Drawing.Point(140, 426);
            this.removeAlbumTextBox.Name = "removeAlbumTextBox";
            this.removeAlbumTextBox.Size = new System.Drawing.Size(119, 22);
            this.removeAlbumTextBox.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(29, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 20);
            this.label5.TabIndex = 20;
            this.label5.Text = "User Name ";
            // 
            // userNameAddTextBox
            // 
            this.userNameAddTextBox.ForeColor = System.Drawing.Color.Black;
            this.userNameAddTextBox.Location = new System.Drawing.Point(140, 149);
            this.userNameAddTextBox.Name = "userNameAddTextBox";
            this.userNameAddTextBox.Size = new System.Drawing.Size(109, 22);
            this.userNameAddTextBox.TabIndex = 21;
            // 
            // removeUsertextBox
            // 
            this.removeUsertextBox.ForeColor = System.Drawing.Color.Black;
            this.removeUsertextBox.Location = new System.Drawing.Point(141, 389);
            this.removeUsertextBox.Name = "removeUsertextBox";
            this.removeUsertextBox.Size = new System.Drawing.Size(118, 22);
            this.removeUsertextBox.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(29, 389);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 20);
            this.label6.TabIndex = 23;
            this.label6.Text = "User Name ";
            // 
            // openUserTextBox
            // 
            this.openUserTextBox.ForeColor = System.Drawing.Color.Black;
            this.openUserTextBox.Location = new System.Drawing.Point(441, 149);
            this.openUserTextBox.Name = "openUserTextBox";
            this.openUserTextBox.Size = new System.Drawing.Size(104, 22);
            this.openUserTextBox.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(336, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 20);
            this.label7.TabIndex = 25;
            this.label7.Text = "User Name ";
            // 
            // addAlbum
            // 
            this.addAlbum.Image = ((System.Drawing.Image)(resources.GetObject("addAlbum.Image")));
            this.addAlbum.Location = new System.Drawing.Point(21, 20);
            this.addAlbum.Name = "addAlbum";
            this.addAlbum.Size = new System.Drawing.Size(250, 200);
            this.addAlbum.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.addAlbum.TabIndex = 5;
            this.addAlbum.TabStop = false;
            // 
            // openAlbum
            // 
            this.openAlbum.Image = ((System.Drawing.Image)(resources.GetObject("openAlbum.Image")));
            this.openAlbum.Location = new System.Drawing.Point(313, 20);
            this.openAlbum.Name = "openAlbum";
            this.openAlbum.Size = new System.Drawing.Size(250, 200);
            this.openAlbum.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.openAlbum.TabIndex = 4;
            this.openAlbum.TabStop = false;
            // 
            // listOfAllAlbum
            // 
            this.listOfAllAlbum.Image = ((System.Drawing.Image)(resources.GetObject("listOfAllAlbum.Image")));
            this.listOfAllAlbum.Location = new System.Drawing.Point(606, 20);
            this.listOfAllAlbum.Name = "listOfAllAlbum";
            this.listOfAllAlbum.Size = new System.Drawing.Size(250, 200);
            this.listOfAllAlbum.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.listOfAllAlbum.TabIndex = 3;
            this.listOfAllAlbum.TabStop = false;
            // 
            // listAlbumOfUser
            // 
            this.listAlbumOfUser.Image = ((System.Drawing.Image)(resources.GetObject("listAlbumOfUser.Image")));
            this.listAlbumOfUser.Location = new System.Drawing.Point(606, 261);
            this.listAlbumOfUser.Name = "listAlbumOfUser";
            this.listAlbumOfUser.Size = new System.Drawing.Size(250, 200);
            this.listAlbumOfUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.listAlbumOfUser.TabIndex = 2;
            this.listAlbumOfUser.TabStop = false;
            // 
            // closeAlbum
            // 
            this.closeAlbum.Image = ((System.Drawing.Image)(resources.GetObject("closeAlbum.Image")));
            this.closeAlbum.Location = new System.Drawing.Point(313, 261);
            this.closeAlbum.Name = "closeAlbum";
            this.closeAlbum.Size = new System.Drawing.Size(250, 200);
            this.closeAlbum.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.closeAlbum.TabIndex = 1;
            this.closeAlbum.TabStop = false;
            // 
            // removeAlbum
            // 
            this.removeAlbum.Image = ((System.Drawing.Image)(resources.GetObject("removeAlbum.Image")));
            this.removeAlbum.Location = new System.Drawing.Point(21, 261);
            this.removeAlbum.Name = "removeAlbum";
            this.removeAlbum.Size = new System.Drawing.Size(250, 200);
            this.removeAlbum.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.removeAlbum.TabIndex = 0;
            this.removeAlbum.TabStop = false;
            // 
            // AlbumCustomControler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.openUserTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.removeUsertextBox);
            this.Controls.Add(this.userNameAddTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.removeAlbumTextBox);
            this.Controls.Add(this.UserListAlbumtextBox);
            this.Controls.Add(this.albumOpenTextBox);
            this.Controls.Add(this.createAlbumTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listAlbumOfUser_b);
            this.Controls.Add(this.closeAlbum_b);
            this.Controls.Add(this.openAlbum_b);
            this.Controls.Add(this.listOfAllAlbum_b);
            this.Controls.Add(this.addAlbum_b);
            this.Controls.Add(this.removeAlbum_b);
            this.Controls.Add(this.addAlbum);
            this.Controls.Add(this.openAlbum);
            this.Controls.Add(this.listOfAllAlbum);
            this.Controls.Add(this.listAlbumOfUser);
            this.Controls.Add(this.closeAlbum);
            this.Controls.Add(this.removeAlbum);
            this.Name = "AlbumCustomControler";
            this.Size = new System.Drawing.Size(879, 482);
            ((System.ComponentModel.ISupportInitialize)(this.addAlbum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.openAlbum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listOfAllAlbum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listAlbumOfUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeAlbum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.removeAlbum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.PictureBox removeAlbum;
        private System.Windows.Forms.PictureBox closeAlbum;
        private System.Windows.Forms.PictureBox listAlbumOfUser;
        private System.Windows.Forms.PictureBox listOfAllAlbum;
        private System.Windows.Forms.PictureBox openAlbum;
        private System.Windows.Forms.PictureBox addAlbum;
        private System.Windows.Forms.Button removeAlbum_b;
        private System.Windows.Forms.Button addAlbum_b;
        private System.Windows.Forms.Button listOfAllAlbum_b;
        private System.Windows.Forms.Button openAlbum_b;
        private System.Windows.Forms.Button closeAlbum_b;
        private System.Windows.Forms.Button listAlbumOfUser_b;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox createAlbumTextBox;
        private System.Windows.Forms.TextBox albumOpenTextBox;
        private System.Windows.Forms.TextBox UserListAlbumtextBox;
        private System.Windows.Forms.TextBox removeAlbumTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox userNameAddTextBox;
        private System.Windows.Forms.TextBox removeUsertextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox openUserTextBox;
        private System.Windows.Forms.Label label7;
    }
}
