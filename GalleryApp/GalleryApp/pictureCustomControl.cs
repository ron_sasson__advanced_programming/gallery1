﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GallaryApp
{
    public partial class pictureCustomControl : UserControl
    {
        private PipeMengmer mengmer;
        private listCustomControler listItem;

        public pictureCustomControl()
        {
            InitializeComponent();
        }

        private void addPicture_Click(object sender, EventArgs e)
        {
            if (addNameT.Text != "" && addPathT.Text != "")
            {
                try
                {
                    mengmer.addPictureToAlbum(addNameT.Text, addPathT.Text);
                    MessageBox.Show("Picture add successfully");
                }
                catch (Exception)
                {
                    MessageBox.Show("This name are exsist, try anther name");
                }

                addNameT.Text = "";
                addPathT.Text = "";
            }
        }
        private void removePicture_Click(object sender, EventArgs e)
        {
            if (removeNameT.Text != "")
            {
                try
                {
                    mengmer.removePictureFromAlbum(removeNameT.Text);
                    MessageBox.Show("Picture remove successfully");
                }
                catch (Exception)
                {
                    MessageBox.Show("This name not exsist");
                }

                removeNameT.Text = "";
            }
        }

        private void ChangeName_Click(object sender, EventArgs e)
        {
            if (oldNameT.Text != "" && newNameT.Text != "")
            {
                try
                {
                    mengmer.changePicture(oldNameT.Text, newNameT.Text);
                    MessageBox.Show("Picture change is name successfully");
                }
                catch (Exception)
                {
                    MessageBox.Show("This name not exsist\nor the new name is exsist");
                }

                newNameT.Text = "";
                oldNameT.Text = "";
            }
        }

        private void edit_Click(object sender, EventArgs e)
        {
            if (editNameT.Text != "")
            {
                try
                {
                    mengmer.showPicture(editNameT.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("This name not exsist");
                }

                editNameT.Text = "";
            }
        }

        private void listPicture_Click(object sender, EventArgs e)
        {
            listItem.populateItem(mengmer.listPicturesInAlbum(), "picture");
            listItem.BringToFront();
        }

        public void setListItem(listCustomControler listItem)
        {
            this.listItem = listItem;
        }

        public void SetMengmer(PipeMengmer mengmer)
        {
            this.mengmer = mengmer;
        }
    }
}
