﻿namespace GallaryApp
{
    partial class pictureCustomControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(pictureCustomControl));
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.oldNameT = new System.Windows.Forms.TextBox();
            this.newNameT = new System.Windows.Forms.TextBox();
            this.editNameT = new System.Windows.Forms.TextBox();
            this.addNameT = new System.Windows.Forms.TextBox();
            this.addPathT = new System.Windows.Forms.TextBox();
            this.removeNameT = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listPicture = new System.Windows.Forms.Button();
            this.edit = new System.Windows.Forms.Button();
            this.ChangeName = new System.Windows.Forms.Button();
            this.addPicture = new System.Windows.Forms.Button();
            this.removePicture = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(48, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "New Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(212, 418);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "Path";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(205, 385);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Picture name";
            // 
            // oldNameT
            // 
            this.oldNameT.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.oldNameT.ForeColor = System.Drawing.Color.Black;
            this.oldNameT.Location = new System.Drawing.Point(168, 154);
            this.oldNameT.Name = "oldNameT";
            this.oldNameT.Size = new System.Drawing.Size(113, 22);
            this.oldNameT.TabIndex = 14;
            // 
            // newNameT
            // 
            this.newNameT.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.newNameT.ForeColor = System.Drawing.Color.Black;
            this.newNameT.Location = new System.Drawing.Point(168, 182);
            this.newNameT.Name = "newNameT";
            this.newNameT.Size = new System.Drawing.Size(113, 22);
            this.newNameT.TabIndex = 15;
            // 
            // editNameT
            // 
            this.editNameT.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.editNameT.ForeColor = System.Drawing.Color.Black;
            this.editNameT.Location = new System.Drawing.Point(511, 182);
            this.editNameT.Name = "editNameT";
            this.editNameT.Size = new System.Drawing.Size(110, 22);
            this.editNameT.TabIndex = 16;
            // 
            // addNameT
            // 
            this.addNameT.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.addNameT.ForeColor = System.Drawing.Color.Black;
            this.addNameT.Location = new System.Drawing.Point(331, 385);
            this.addNameT.Name = "addNameT";
            this.addNameT.Size = new System.Drawing.Size(110, 22);
            this.addNameT.TabIndex = 17;
            // 
            // addPathT
            // 
            this.addPathT.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.addPathT.ForeColor = System.Drawing.Color.Black;
            this.addPathT.Location = new System.Drawing.Point(331, 418);
            this.addPathT.Name = "addPathT";
            this.addPathT.Size = new System.Drawing.Size(110, 22);
            this.addPathT.TabIndex = 18;
            // 
            // removeNameT
            // 
            this.removeNameT.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.removeNameT.ForeColor = System.Drawing.Color.Black;
            this.removeNameT.Location = new System.Drawing.Point(662, 418);
            this.removeNameT.Name = "removeNameT";
            this.removeNameT.Size = new System.Drawing.Size(110, 22);
            this.removeNameT.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(536, 418);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 20);
            this.label7.TabIndex = 20;
            this.label7.Text = "Picture name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(385, 182);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 20);
            this.label8.TabIndex = 21;
            this.label8.Text = "Picture name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(42, 154);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 20);
            this.label1.TabIndex = 22;
            this.label1.Text = "Picture name";
            // 
            // listPicture
            // 
            this.listPicture.BackColor = System.Drawing.Color.White;
            this.listPicture.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.listPicture.ForeColor = System.Drawing.Color.Black;
            this.listPicture.Location = new System.Drawing.Point(786, 37);
            this.listPicture.Name = "listPicture";
            this.listPicture.Size = new System.Drawing.Size(121, 50);
            this.listPicture.TabIndex = 23;
            this.listPicture.Text = "List Picture In Album";
            this.listPicture.UseVisualStyleBackColor = false;
            this.listPicture.Click += new System.EventHandler(this.listPicture_Click);
            // 
            // edit
            // 
            this.edit.BackColor = System.Drawing.Color.White;
            this.edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.edit.ForeColor = System.Drawing.Color.Black;
            this.edit.Location = new System.Drawing.Point(436, 37);
            this.edit.Name = "edit";
            this.edit.Size = new System.Drawing.Size(116, 37);
            this.edit.TabIndex = 24;
            this.edit.Text = "Edit Picture";
            this.edit.UseVisualStyleBackColor = false;
            this.edit.Click += new System.EventHandler(this.edit_Click);
            // 
            // ChangeName
            // 
            this.ChangeName.BackColor = System.Drawing.Color.White;
            this.ChangeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.ChangeName.ForeColor = System.Drawing.Color.Black;
            this.ChangeName.Location = new System.Drawing.Point(99, 37);
            this.ChangeName.Name = "ChangeName";
            this.ChangeName.Size = new System.Drawing.Size(123, 50);
            this.ChangeName.TabIndex = 25;
            this.ChangeName.Text = "Change Name Of Picture";
            this.ChangeName.UseVisualStyleBackColor = false;
            this.ChangeName.Click += new System.EventHandler(this.ChangeName_Click);
            // 
            // addPicture
            // 
            this.addPicture.BackColor = System.Drawing.Color.White;
            this.addPicture.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.addPicture.ForeColor = System.Drawing.Color.Black;
            this.addPicture.Location = new System.Drawing.Point(258, 264);
            this.addPicture.Name = "addPicture";
            this.addPicture.Size = new System.Drawing.Size(116, 37);
            this.addPicture.TabIndex = 26;
            this.addPicture.Text = "Add Picture";
            this.addPicture.UseVisualStyleBackColor = false;
            this.addPicture.Click += new System.EventHandler(this.addPicture_Click);
            // 
            // removePicture
            // 
            this.removePicture.BackColor = System.Drawing.Color.White;
            this.removePicture.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.removePicture.ForeColor = System.Drawing.Color.Black;
            this.removePicture.Location = new System.Drawing.Point(600, 264);
            this.removePicture.Name = "removePicture";
            this.removePicture.Size = new System.Drawing.Size(125, 45);
            this.removePicture.TabIndex = 27;
            this.removePicture.Text = "Remove Picture";
            this.removePicture.UseVisualStyleBackColor = false;
            this.removePicture.Click += new System.EventHandler(this.removePicture_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(200, 242);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(241, 209);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(531, 242);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(241, 209);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 29;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(40, 14);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(241, 209);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 30;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(380, 14);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(241, 209);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 31;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(722, 14);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(241, 209);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 32;
            this.pictureBox5.TabStop = false;
            // 
            // pictureCustomControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.removePicture);
            this.Controls.Add(this.addPicture);
            this.Controls.Add(this.ChangeName);
            this.Controls.Add(this.edit);
            this.Controls.Add(this.listPicture);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.removeNameT);
            this.Controls.Add(this.addPathT);
            this.Controls.Add(this.addNameT);
            this.Controls.Add(this.editNameT);
            this.Controls.Add(this.newNameT);
            this.Controls.Add(this.oldNameT);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Name = "pictureCustomControl";
            this.Size = new System.Drawing.Size(978, 482);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox oldNameT;
        private System.Windows.Forms.TextBox newNameT;
        private System.Windows.Forms.TextBox editNameT;
        private System.Windows.Forms.TextBox addNameT;
        private System.Windows.Forms.TextBox addPathT;
        private System.Windows.Forms.TextBox removeNameT;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button listPicture;
        public System.Windows.Forms.Button edit;
        public System.Windows.Forms.Button ChangeName;
        public System.Windows.Forms.Button addPicture;
        public System.Windows.Forms.Button removePicture;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
    }
}
