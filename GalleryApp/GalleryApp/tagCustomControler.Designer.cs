﻿namespace GallaryApp
{
    partial class tagCustomControler
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(tagCustomControler));
            this.tagUser = new System.Windows.Forms.Button();
            this.unTagsUser = new System.Windows.Forms.Button();
            this.listUserTags = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.unTagUsertextBox = new System.Windows.Forms.TextBox();
            this.tagUsertextBox = new System.Windows.Forms.TextBox();
            this.ListTagstextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tagPicturetextBox = new System.Windows.Forms.TextBox();
            this.unTagPicturetextBox = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // tagUser
            // 
            this.tagUser.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.tagUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.tagUser.ForeColor = System.Drawing.Color.Black;
            this.tagUser.Location = new System.Drawing.Point(82, 45);
            this.tagUser.Name = "tagUser";
            this.tagUser.Size = new System.Drawing.Size(129, 52);
            this.tagUser.TabIndex = 4;
            this.tagUser.Text = "Tags user";
            this.tagUser.UseVisualStyleBackColor = true;
            this.tagUser.Click += new System.EventHandler(this.tagUser_Click);
            // 
            // unTagsUser
            // 
            this.unTagsUser.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.unTagsUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.unTagsUser.ForeColor = System.Drawing.Color.Black;
            this.unTagsUser.Location = new System.Drawing.Point(379, 45);
            this.unTagsUser.Name = "unTagsUser";
            this.unTagsUser.Size = new System.Drawing.Size(129, 52);
            this.unTagsUser.TabIndex = 5;
            this.unTagsUser.Text = "Untag user";
            this.unTagsUser.UseVisualStyleBackColor = true;
            this.unTagsUser.Click += new System.EventHandler(this.unTagsUser_Click);
            // 
            // listUserTags
            // 
            this.listUserTags.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.listUserTags.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.listUserTags.ForeColor = System.Drawing.Color.Black;
            this.listUserTags.Location = new System.Drawing.Point(681, 45);
            this.listUserTags.Name = "listUserTags";
            this.listUserTags.Size = new System.Drawing.Size(129, 52);
            this.listUserTags.TabIndex = 6;
            this.listUserTags.Text = "List User Tag In Picture";
            this.listUserTags.UseVisualStyleBackColor = true;
            this.listUserTags.Click += new System.EventHandler(this.listUserTags_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(620, 369);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 20);
            this.label7.TabIndex = 26;
            this.label7.Text = "Picture Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(323, 369);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "User Name ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(29, 369);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 20);
            this.label2.TabIndex = 28;
            this.label2.Text = "User Name ";
            // 
            // unTagUsertextBox
            // 
            this.unTagUsertextBox.ForeColor = System.Drawing.Color.Black;
            this.unTagUsertextBox.Location = new System.Drawing.Point(440, 369);
            this.unTagUsertextBox.Name = "unTagUsertextBox";
            this.unTagUsertextBox.Size = new System.Drawing.Size(117, 22);
            this.unTagUsertextBox.TabIndex = 32;
            // 
            // tagUsertextBox
            // 
            this.tagUsertextBox.ForeColor = System.Drawing.Color.Black;
            this.tagUsertextBox.Location = new System.Drawing.Point(146, 369);
            this.tagUsertextBox.Name = "tagUsertextBox";
            this.tagUsertextBox.Size = new System.Drawing.Size(117, 22);
            this.tagUsertextBox.TabIndex = 33;
            // 
            // ListTagstextBox
            // 
            this.ListTagstextBox.ForeColor = System.Drawing.Color.Black;
            this.ListTagstextBox.Location = new System.Drawing.Point(734, 369);
            this.ListTagstextBox.Name = "ListTagstextBox";
            this.ListTagstextBox.Size = new System.Drawing.Size(117, 22);
            this.ListTagstextBox.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(323, 418);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 20);
            this.label3.TabIndex = 35;
            this.label3.Text = "Picture Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(29, 418);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 20);
            this.label4.TabIndex = 36;
            this.label4.Text = "Picture Name";
            // 
            // tagPicturetextBox
            // 
            this.tagPicturetextBox.ForeColor = System.Drawing.Color.Black;
            this.tagPicturetextBox.Location = new System.Drawing.Point(146, 418);
            this.tagPicturetextBox.Name = "tagPicturetextBox";
            this.tagPicturetextBox.Size = new System.Drawing.Size(117, 22);
            this.tagPicturetextBox.TabIndex = 37;
            // 
            // unTagPicturetextBox
            // 
            this.unTagPicturetextBox.ForeColor = System.Drawing.Color.Black;
            this.unTagPicturetextBox.Location = new System.Drawing.Point(440, 416);
            this.unTagPicturetextBox.Name = "unTagPicturetextBox";
            this.unTagPicturetextBox.Size = new System.Drawing.Size(117, 22);
            this.unTagPicturetextBox.TabIndex = 38;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(18, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 429);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 39;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(311, 26);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(256, 429);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 40;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(604, 26);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(256, 429);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 41;
            this.pictureBox3.TabStop = false;
            // 
            // tagCustomControler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.Controls.Add(this.unTagPicturetextBox);
            this.Controls.Add(this.tagPicturetextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ListTagstextBox);
            this.Controls.Add(this.tagUsertextBox);
            this.Controls.Add(this.unTagUsertextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.listUserTags);
            this.Controls.Add(this.unTagsUser);
            this.Controls.Add(this.tagUser);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Name = "tagCustomControler";
            this.Size = new System.Drawing.Size(879, 482);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button tagUser;
        private System.Windows.Forms.Button unTagsUser;
        private System.Windows.Forms.Button listUserTags;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox unTagUsertextBox;
        private System.Windows.Forms.TextBox tagUsertextBox;
        private System.Windows.Forms.TextBox ListTagstextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tagPicturetextBox;
        private System.Windows.Forms.TextBox unTagPicturetextBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}
