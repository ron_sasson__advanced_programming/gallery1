﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GallaryApp
{
    public partial class tagCustomControler : UserControl
    {
        private PipeMengmer mengmer;
        private listCustomControler listItem;

        public tagCustomControler()
        {
            InitializeComponent();
        }

        private void tagUser_Click(object sender, EventArgs e)
        {
            int userId = 0;
            bool checkUserExsist = true;
            if (tagUsertextBox.Text != "" && tagPicturetextBox.Text != "")
            {
                try
                {
                    userId = mengmer.getUserId(tagUsertextBox.Text); // get user id
                }
                catch (Exception) // if user id not exsist
                {
                    MessageBox.Show("The user not exsist");
                    checkUserExsist = false;
                }

                if (checkUserExsist)
                {
                    try
                    {
                        mengmer.tagUserInPicture(tagPicturetextBox.Text, userId);
                        MessageBox.Show("user tag successfully");
                    }
                    catch (Exception)
                    {
                        // it mean the user are axsist
                        MessageBox.Show("The picture not exsist");
                    }
                }

                tagPicturetextBox.Text = "";
                tagUsertextBox.Text = "";
            }
        }

        private void unTagsUser_Click(object sender, EventArgs e)
        {
            int userId = 0;
            bool checkUserExsist = true;
            if (unTagUsertextBox.Text != "" && unTagPicturetextBox.Text != "")
            {
                try
                {
                    userId = mengmer.getUserId(unTagUsertextBox.Text); // get user id
                }
                catch (Exception) // if user id not exsist
                {
                    MessageBox.Show("The user not exsist");
                    checkUserExsist = false;
                }

                if (checkUserExsist)
                {
                    try
                    {
                        mengmer.untagUserInPicture(unTagPicturetextBox.Text, userId);
                        MessageBox.Show("user untag successfully");
                    }
                    catch (Exception)
                    {
                        // it mean the user are axsist
                        MessageBox.Show("The picture not exsist or the user not tag");
                    }
                }

                unTagPicturetextBox.Text = "";
                unTagUsertextBox.Text = "";
            }
        }

        private void listUserTags_Click(object sender, EventArgs e)
        {
            if (ListTagstextBox.Text != "")
            {
                try
                {
                    listItem.populateItem(mengmer.listUserTags(ListTagstextBox.Text), "tags");
                    listItem.BringToFront();
                }
                catch (Exception)
                {
                    MessageBox.Show("The picture not exsist");
                }
            }
        }

        public void setListItem(listCustomControler listItem)
        {
            this.listItem = listItem;
        }

        public void SetMengmer(PipeMengmer mengmer)
        {
            this.mengmer = mengmer;
        }


    }
}
