﻿namespace GallaryApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel3 = new System.Windows.Forms.Panel();
            this.pict = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.sidePanel = new System.Windows.Forms.Panel();
            this.Exit = new System.Windows.Forms.Button();
            this.Statistics = new System.Windows.Forms.Button();
            this.Tags = new System.Windows.Forms.Button();
            this.Albums = new System.Windows.Forms.Button();
            this.Users = new System.Windows.Forms.Button();
            this.home = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.homeCustomConroler1 = new GallaryApp.homeCustomConroler();
            this.listCustomControler1 = new GallaryApp.listCustomControler();
            this.albumCustomControler1 = new GallaryApp.AlbumCustomControler();
            this.userControl11 = new GallaryApp.UserControl1();
            this.tagCustomControler1 = new GallaryApp.tagCustomControler();
            this.statisticsCustomControl1 = new GallaryApp.StatisticsCustomControl();
            this.pictureCustomControl1 = new GallaryApp.pictureCustomControl();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(39)))));
            this.panel3.Controls.Add(this.pict);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.sidePanel);
            this.panel3.Controls.Add(this.Exit);
            this.panel3.Controls.Add(this.Statistics);
            this.panel3.Controls.Add(this.Tags);
            this.panel3.Controls.Add(this.Albums);
            this.panel3.Controls.Add(this.Users);
            this.panel3.Controls.Add(this.home);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(255, 742);
            this.panel3.TabIndex = 1;
            // 
            // pict
            // 
            this.pict.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pict.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.pict.Image = ((System.Drawing.Image)(resources.GetObject("pict.Image")));
            this.pict.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.pict.Location = new System.Drawing.Point(3, 306);
            this.pict.Name = "pict";
            this.pict.Size = new System.Drawing.Size(249, 80);
            this.pict.TabIndex = 7;
            this.pict.Text = "       Pictures";
            this.pict.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.pict.UseVisualStyleBackColor = true;
            this.pict.Click += new System.EventHandler(this.pict_Click);
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(255, 242);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1092, 497);
            this.panel4.TabIndex = 5;
            // 
            // sidePanel
            // 
            this.sidePanel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.sidePanel.ForeColor = System.Drawing.Color.OldLace;
            this.sidePanel.Location = new System.Drawing.Point(6, 140);
            this.sidePanel.Name = "sidePanel";
            this.sidePanel.Size = new System.Drawing.Size(7, 75);
            this.sidePanel.TabIndex = 4;
            // 
            // Exit
            // 
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Font = new System.Drawing.Font("Bauhaus 93", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Exit.Image = ((System.Drawing.Image)(resources.GetObject("Exit.Image")));
            this.Exit.Location = new System.Drawing.Point(6, 696);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(40, 43);
            this.Exit.TabIndex = 4;
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // Statistics
            // 
            this.Statistics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Statistics.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Statistics.Image = ((System.Drawing.Image)(resources.GetObject("Statistics.Image")));
            this.Statistics.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Statistics.Location = new System.Drawing.Point(6, 564);
            this.Statistics.Name = "Statistics";
            this.Statistics.Size = new System.Drawing.Size(249, 80);
            this.Statistics.TabIndex = 4;
            this.Statistics.Text = "       Statistics";
            this.Statistics.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Statistics.UseVisualStyleBackColor = true;
            this.Statistics.Click += new System.EventHandler(this.Statistics_Click);
            // 
            // Tags
            // 
            this.Tags.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Tags.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Tags.Image = ((System.Drawing.Image)(resources.GetObject("Tags.Image")));
            this.Tags.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Tags.Location = new System.Drawing.Point(6, 478);
            this.Tags.Name = "Tags";
            this.Tags.Size = new System.Drawing.Size(249, 80);
            this.Tags.TabIndex = 3;
            this.Tags.Text = "       Tags";
            this.Tags.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Tags.UseVisualStyleBackColor = true;
            this.Tags.Click += new System.EventHandler(this.Tags_Click);
            // 
            // Albums
            // 
            this.Albums.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Albums.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Albums.Image = ((System.Drawing.Image)(resources.GetObject("Albums.Image")));
            this.Albums.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Albums.Location = new System.Drawing.Point(6, 221);
            this.Albums.Name = "Albums";
            this.Albums.Size = new System.Drawing.Size(249, 80);
            this.Albums.TabIndex = 2;
            this.Albums.Text = "       Albums";
            this.Albums.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Albums.UseVisualStyleBackColor = true;
            this.Albums.Click += new System.EventHandler(this.Albums_Click);
            // 
            // Users
            // 
            this.Users.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Users.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Users.Image = ((System.Drawing.Image)(resources.GetObject("Users.Image")));
            this.Users.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Users.Location = new System.Drawing.Point(6, 392);
            this.Users.Name = "Users";
            this.Users.Size = new System.Drawing.Size(249, 80);
            this.Users.TabIndex = 1;
            this.Users.Text = "       User";
            this.Users.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Users.UseVisualStyleBackColor = true;
            this.Users.Click += new System.EventHandler(this.Users_Click);
            // 
            // home
            // 
            this.home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.home.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.home.Image = ((System.Drawing.Image)(resources.GetObject("home.Image")));
            this.home.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.home.Location = new System.Drawing.Point(6, 135);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(249, 80);
            this.home.TabIndex = 0;
            this.home.Text = "       Home";
            this.home.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.home.UseVisualStyleBackColor = true;
            this.home.Click += new System.EventHandler(this.home_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkBlue;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(255, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1134, 11);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkBlue;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Location = new System.Drawing.Point(309, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(192, 235);
            this.panel2.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 162);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 37);
            this.label1.TabIndex = 4;
            this.label1.Text = "Gallary app";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-4, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(197, 158);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(892, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(403, 118);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // homeCustomConroler1
            // 
            this.homeCustomConroler1.BackColor = System.Drawing.Color.Silver;
            this.homeCustomConroler1.Location = new System.Drawing.Point(254, 246);
            this.homeCustomConroler1.Margin = new System.Windows.Forms.Padding(4);
            this.homeCustomConroler1.Name = "homeCustomConroler1";
            this.homeCustomConroler1.Size = new System.Drawing.Size(1132, 500);
            this.homeCustomConroler1.TabIndex = 5;
            // 
            // listCustomControler1
            // 
            this.listCustomControler1.Location = new System.Drawing.Point(254, 258);
            this.listCustomControler1.Margin = new System.Windows.Forms.Padding(4);
            this.listCustomControler1.Name = "listCustomControler1";
            this.listCustomControler1.Size = new System.Drawing.Size(1134, 488);
            this.listCustomControler1.TabIndex = 7;
            // 
            // albumCustomControler1
            // 
            this.albumCustomControler1.BackColor = System.Drawing.Color.Silver;
            this.albumCustomControler1.Location = new System.Drawing.Point(255, 246);
            this.albumCustomControler1.Margin = new System.Windows.Forms.Padding(4);
            this.albumCustomControler1.Name = "albumCustomControler1";
            this.albumCustomControler1.Size = new System.Drawing.Size(1134, 496);
            this.albumCustomControler1.TabIndex = 8;
            // 
            // userControl11
            // 
            this.userControl11.BackColor = System.Drawing.Color.Silver;
            this.userControl11.Location = new System.Drawing.Point(254, 246);
            this.userControl11.Margin = new System.Windows.Forms.Padding(4);
            this.userControl11.Name = "userControl11";
            this.userControl11.Size = new System.Drawing.Size(1177, 498);
            this.userControl11.TabIndex = 6;
            // 
            // tagCustomControler1
            // 
            this.tagCustomControler1.BackColor = System.Drawing.Color.Silver;
            this.tagCustomControler1.Location = new System.Drawing.Point(254, 246);
            this.tagCustomControler1.Margin = new System.Windows.Forms.Padding(4);
            this.tagCustomControler1.Name = "tagCustomControler1";
            this.tagCustomControler1.Size = new System.Drawing.Size(1132, 500);
            this.tagCustomControler1.TabIndex = 9;
            // 
            // statisticsCustomControl1
            // 
            this.statisticsCustomControl1.BackColor = System.Drawing.Color.Silver;
            this.statisticsCustomControl1.Location = new System.Drawing.Point(254, 242);
            this.statisticsCustomControl1.Margin = new System.Windows.Forms.Padding(4);
            this.statisticsCustomControl1.Name = "statisticsCustomControl1";
            this.statisticsCustomControl1.Size = new System.Drawing.Size(1134, 500);
            this.statisticsCustomControl1.TabIndex = 10;
            // 
            // pictureCustomControl1
            // 
            this.pictureCustomControl1.Location = new System.Drawing.Point(254, 242);
            this.pictureCustomControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureCustomControl1.Name = "pictureCustomControl1";
            this.pictureCustomControl1.Size = new System.Drawing.Size(1135, 502);
            this.pictureCustomControl1.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1389, 742);
            this.Controls.Add(this.pictureCustomControl1);
            this.Controls.Add(this.statisticsCustomControl1);
            this.Controls.Add(this.tagCustomControler1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.homeCustomConroler1);
            this.Controls.Add(this.listCustomControler1);
            this.Controls.Add(this.albumCustomControler1);
            this.Controls.Add(this.userControl11);
            this.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Tags;
        private System.Windows.Forms.Button Albums;
        private System.Windows.Forms.Button Users;
        private System.Windows.Forms.Button home;
        private System.Windows.Forms.Button Statistics;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Panel sidePanel;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel4;
        private homeCustomConroler homeCustomConroler1;
        private System.Windows.Forms.Button pict;
        private UserControl1 userControl11;
        private listCustomControler listCustomControler1;
        private AlbumCustomControler albumCustomControler1;
        private tagCustomControler tagCustomControler1;
        private StatisticsCustomControl statisticsCustomControl1;
        private pictureCustomControl pictureCustomControl1;
    }
}

