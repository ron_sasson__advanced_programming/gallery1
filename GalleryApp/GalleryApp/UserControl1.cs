﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GallaryApp
{
    public partial class UserControl1 : UserControl
    {
        private PipeMengmer mengmer;
        private listCustomControler listItem;

        public UserControl1()
        {
            InitializeComponent();
        }

        private void addUser_Click(object sender, EventArgs e)
        {
            if (addUserTextBox.Text != "")
            {
                try
                {
                    mengmer.addUser(addUserTextBox.Text);
                    MessageBox.Show("user add successfully");
                }
                catch (Exception)
                {
                    // it mean the user are axsist
                    MessageBox.Show("The user exsist");
                }

                addUserTextBox.Text = "";
            }
        }

        private void RemoveUser_Click(object sender, EventArgs e)
        {
            int id = 0;
            bool checkUserExsist = false;
            if (RemoveUserTextBox.Text != "")
            {
                
                try
                {
                    id = mengmer.getUserId(RemoveUserTextBox.Text);
                    MessageBox.Show("User remove successfully");
                    checkUserExsist = true;
                }
                catch (Exception)
                {
                    MessageBox.Show("The user not exsist");
                }

                if (checkUserExsist)
                {
                    try
                    {
                        mengmer.removeUser(id);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("have problem try agin");
                    }
                }
                RemoveUserTextBox.Text = "";
            }
        }

        private void statistic_Click(object sender, EventArgs e)
        {
            int userId = 0;
            bool checkUserExsist = true;
            if(statisticUserTextBox.Text != "")
            {

                try
                {
                    userId = mengmer.getUserId(statisticUserTextBox.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("The user not exsist");
                    checkUserExsist = false;
                }

                if(checkUserExsist)
                {
                    try
                    {
                        string statistic = "";
                        List<string> statisticList =  mengmer.userStatistics(userId);

                        statistic += "Count of Albums Tagged: " + statisticList.ElementAt(0) + "\n";
                        statistic += "Count of Tags: " + statisticList.ElementAt(1) + "\n";
                        statistic += "Avarage Tags per Alboum: " + statisticList.ElementAt(2) + "\n";

                        MessageBox.Show(statistic);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("have problem try agin");
                    }

                    statisticUserTextBox.Text = "";
                }

            }
        }

        private void listUsers_Click(object sender, EventArgs e)
        {
            listItem.populateItem(mengmer.listUsers(), "user");
            listItem.BringToFront();
        }

        public void setListItem(listCustomControler listItem)
        {
            this.listItem = listItem;
        }

        public void SetMengmer(PipeMengmer mengmer)
        {
            this.mengmer = mengmer;
        }



        private void addUserTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

       
    }
}
