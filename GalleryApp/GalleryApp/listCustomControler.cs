﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GalleryApp.Properties;
using System.IO;

namespace GallaryApp
{
    public partial class listCustomControler : UserControl
    {
        public listCustomControler()
        {
            InitializeComponent();           
        }

        public void populateItem(List<List<string>> listOf, string whatStringItIs)
        {
            flowLayoutPanel1.Controls.Clear();

            if (listOf.Count() > 0)
            {
                listViewCustomContoroler[] listItem = new listViewCustomContoroler[listOf.Count()];
                string[] id = new string[listOf.Count()];
                string[] parameters = new string[listOf.Count()];
                string[] path = new string[listOf.Count()]; // used only in picture list

                for (int i = 0; i < listItem.Length; i++) // get all parameter of user
                {
                    id[i] = listOf.ElementAt(i).ElementAt(0); // get id
                    for (int j = 1; j < listOf[i].Count(); j++)
                    {
                        if (whatStringItIs == "picture" && j == 2) // in i = 2 we get the path
                            path[i] = listOf.ElementAt(i).ElementAt(j);
                        else
                            parameters[i] += listOf.ElementAt(i).ElementAt(j) + "\n"; // get all parameter
                    }
                }

                for (int i = 0; i < listItem.Length; i++)
                {
                    listItem[i] = new listViewCustomContoroler();
                    listItem[i].Id = id[i];
                    listItem[i].Parameters = parameters[i];

                    if (whatStringItIs == "album")
                    {
                        listItem[i].Icon = Resources.album;
                    }
                    else if (whatStringItIs == "user")
                    {
                        listItem[i].Icon = Resources.users;
                    }
                    else if (whatStringItIs == "tags")
                    {
                        listItem[i].Icon = Resources.tag;
                    }
                    else if (whatStringItIs == "picture")
                    {
                        if(File.Exists(path[i]))
                        {
                            listItem[i].Icon = Image.FromFile(path[i]);
                        }
                    }


                        if (flowLayoutPanel1.Controls.Count < 0)
                        {
                            flowLayoutPanel1.Controls.Clear();
                        }

                    flowLayoutPanel1.Controls.Add(listItem[i]);

                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.SendToBack();
        }
    }
}
