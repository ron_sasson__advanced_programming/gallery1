﻿using System;
using System.Collections.Generic;
using System.Text;


namespace GallaryApp
{
    public class PipeMengmer
    {
        private pipe _pipe;
		private string _msgFromClient;
		private bool albumOpen;


		public PipeMengmer(pipe newPipe)
        {
            _pipe = newPipe;
			_pipe.connect();
        }

		// albums management
		public List<string> createAlbum(int userId, string newName) //code 1
		{
			//return list of album we are create
			string msg = "01";
			msg += userId.ToString() + "|";
			msg += newName + "|";

			sendGetMesge(msg);

			List<string> newAlbum = getParameter(_msgFromClient.Substring(4));
			albumOpen = false;

			return newAlbum;
		}
		public void openAlbum(int userId, string newName) //code 2
		{
			string msg = "02";
			msg += userId.ToString() + "|";
			msg += newName + "|";

			sendGetMesge(msg);
		}
		public void closeAlbum() //code 3
		{
			string msg = "03";

			sendGetMesge(msg);
		}
		public void deleteAlbum(int userId, string newName) //code 4
		{
			string msg = "04";
			msg += userId.ToString() + "|";
			msg += newName + "|";

			sendGetMesge(msg);
		}
		public List<List<string>> listAlbums() //code 5
		{
			string msg = "05";

			sendGetMesge(msg);

			List<List<string>> allAlbum = getList(_msgFromClient.Substring(4));

			return allAlbum;
		}
		public List<List<string>> listAlbumsOfUser(int userId) //code 6
		{
			string msg = "06";
			msg += userId.ToString() + "|";

			sendGetMesge(msg);

			List<List<string>> allAlbumOfUser = getList(_msgFromClient.Substring(4));

			return allAlbumOfUser;
		}

		// Picture management
		public List<string> addPictureToAlbum(string newName, string path) //code 7
		{
			//return the new picture we create
			string msg = "07";
			msg += newName + "|";
			msg += path + "|";

			sendGetMesge(msg);

			return getParameter(_msgFromClient.Substring(4));
		}
		public void removePictureFromAlbum(string name) //code 8
		{
			string msg = "08";
			msg += name + "|";

			sendGetMesge(msg);
		}
		public void showPicture(string name) //code 9  for edit
		{
			string msg = "09";
			msg += name + "|";

			sendGetMesge(msg);
		}
		public List<List<string>> listPicturesInAlbum() //code 10
		{
			string msg = "10";

			sendGetMesge(msg);

			return getList(_msgFromClient.Substring(4));
		}
		public void changePicture(string name, string newName) //code 11
		{
			string msg = "11";
			msg += name + "|";
			msg += newName + "|";

			sendGetMesge(msg);
		}

		// tags related
		public void tagUserInPicture(string namePic, int userId) //code 12
		{
			string msg = "12";
			msg += namePic + "|";
			msg += userId.ToString() + "|";

			sendGetMesge(msg);
		}
		public void untagUserInPicture(string namePic, int userId) //code 13
		{
			string msg = "13";
			msg += namePic + "|";
			msg += userId.ToString() + "|";

			sendGetMesge(msg);
		}
		public List<List<string>> listUserTags(string namePic) //code 14
		{
			string msg = "14";
			msg += namePic + "|";

			sendGetMesge(msg);

			return getList(_msgFromClient.Substring(4));
		}

		// users management
		public int addUser(string newName) //code 15
		{
			//15 is the code of add user
			// if return -999 it mean error	

			string msg = "15";
			msg += newName + "|";
			int userId = 0;

			sendGetMesge(msg);

			List<string> userIdList = getParameter(_msgFromClient.Substring(4));

			foreach (string id in userIdList)
			{
				userId = Int32.Parse(id);
				break;
			}

			return userId;
		}
		public void removeUser(int userId) //code 16
		{
			//16 is the code of add user
			string msg = "16";
			msg += userId.ToString() + "|";

			sendGetMesge(msg);
		}
		public List<List<string>> listUsers() //code 17
		{
			string msg = "17";

			sendGetMesge(msg);

			return getList(_msgFromClient.Substring(4));
		}
		public List<string> userStatistics(int userId) //code 18
		{
			string msg = "18";
			msg += userId.ToString() + "|";

			sendGetMesge(msg);

			return getParameter(_msgFromClient.Substring(4));
		}

		// ******************* Queries ******************* 
		public List<string> topTaggedUser() //code 19
		{
			string msg = "19";

			sendGetMesge(msg);

			return getParameter(_msgFromClient.Substring(4));
		}
		public List<string> topTaggedPicture() //code 20
		{
			string msg = "20";

			sendGetMesge(msg);

			return getParameter(_msgFromClient.Substring(4));
		}
		public List<List<string>> picturesTaggedUser(int userId) //code 21
		{
			string msg = "21";
			msg += userId.ToString() + "|";

			sendGetMesge(msg);

			return getList(_msgFromClient.Substring(4));
		}
		public void exit() //code 0
		{
			_pipe.mesegeToClient("99"); // close the proses
			_pipe.close();
		}


		// help function

		public int getUserId(string name)
		{
			int id = -99;
			

			foreach (List<string> user in listUsers())
			{
				foreach (string parameter in user)
				{
					if (name == parameter) // check if the user for get the id
					{
						foreach (string userId in user) // get id
						{
							if (id == -99)
								id = Int32.Parse(userId);
							break;
						}							
					}
				}

			}

			if (id == -99) // mean the user not exsist
				throw new SystemException();

			return id;
		}

		private List<string> getParameter(string allParameter)
		{
			string[] parametersInArry = allParameter.Split('|');

			List<string> parameters = new List<string>(parametersInArry);

			if (parameters.Contains(""))
				parameters.Remove("");
			
			
			return parameters;
		}

		private List<List<string>> getList(string allElement)
		{
			List<List<string>> elements = new List<List<string>>();
			if (allElement.Length > 0)
			{
				int size = allElement.Length - 1;
				allElement = allElement.Substring(0, size - 1); //delete the last $

				string[] elementInArry = allElement.Split('$');

				foreach (string element in elementInArry)
				{
					elements.Add(getParameter(element));
				}
			}
			return elements;
		}

		private bool checkEror()
		{
			return _msgFromClient.Contains("100#"); // chec if error hepend
		}

		private void sendGetMesge(string msg)
		{
			//this function send and get the mesege from client and input 
			//the vlue in _msgFromClient
			//if throw error there is input worg value

			_pipe.mesegeToClient(msg);
			_msgFromClient = _pipe.getMesgeFromClient();

			if (checkEror()) // if had worng connection
				throw new SystemException("input worng value");
		}

		public bool checkAlbum
		{
			get { return albumOpen; }
			set { albumOpen = value; }
		}
	}
}
